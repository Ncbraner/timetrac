import Vue          from 'vue';
import HistoryApp   from './components/parents/HistoryApp.vue'
import VueResource  from 'vue-resource'

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
const http=Vue.http;
// exporting this allows you to do the http requests in the store
export default http

// Set in .env file to make sure devtools are disabled when live
if (APP_ENV === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug    = false;
    Vue.config.silent   = true;
}

new Vue({
    el: '#historyMain',
    components: { HistoryApp }
});
