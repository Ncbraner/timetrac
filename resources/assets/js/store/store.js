import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        jobs: Ecreativeworks.data,
        resource: Ecreativeworks.resource,
        timeOptions: Ecreativeworks.timeOptions,
        ftp: {},
        conflicting: [],
        showConflicting: false,
        ftpShow: false,
        ftpSpinner: false,
        submitSpinner: false,
        review: false,
        ftpError: false,
        'currentUser': '',
        'selectedFTP': '',
        'productionHours': '',
        'details': '',
        'errorMsg': '',
    },
    mutations: {
        ERROR_HANDLER (state, ErrorStatus) {
            if (ErrorStatus === 'noErr') {
                state.ftpError = false
            } else {
                state.ftpError = true;
                state.errorMsg = ErrorStatus
            }
        },
        TOGGLE_SPINNER (state, spinner) {
            if (spinner === 'ftp') {
                state.ftpSpinner = !state.ftpSpinner;
            } else if (spinner === 'submit') {
                state.submitSpinner = !state.submitSpinner
            }
        },
        INSERT_FTP_INFO (state, ftpObject) {
            state.ftp = ftpObject;
        },
        TOGGLE_FTP_SHOW (state) {
            state.ftpShow = !state.ftpShow;
        },
        SET_ACTIVE_FTP (state, ID) {
            state.selectedFTP = ID;
        },
        SET_USER (state, user) {
            state.currentUser = user;
        },
        EMPTY_CONFLICTING(state) {
            state.conflicting = [];
        },
        INSERT_CONFLICTING(state, conflicting) {
            conflicting.forEach (function (element) {
                state.conflicting.push(element);
            });
        },
        CONFLICTING_MODAL(state) {
            state.showConflicting = !state.showConflicting;
        }
    },
    actions: {
        getProjectFtpInfo: function ({ commit, state }, project) {
            commit('SET_ACTIVE_FTP', project.Id);
            if (state.ftpError === true) {
                commit('ERROR_HANDLER', 'noErr');
            }
            commit('TOGGLE_SPINNER', 'ftp');
            if (state.ftpShow === true) {
                commit('TOGGLE_FTP_SHOW');
            }
            let postData = {
                'id': project.Id
            };
            let token = document.querySelector('#_token').getAttribute('value');
            let headers = {
                'X-CSRF-TOKEN': token
            };
            Vue.http.post('/api/post/projectftp', postData, headers)
                .then(function (response) {
                    let ftp = {};
                    if (response['data']['error']) {
                        commit('ERROR_HANDLER', "There is no Domain attached to this project. Please ask your PM to attach it.");
                        commit('TOGGLE_SPINNER', 'ftp');
                        return;
                    }
                    commit('ERROR_HANDLER', 'noErr');
                    let responseData = response.data[project.Id];
                    ftp.name = project.Name;
                    (project.Project_Description__c !== null) ? ftp.projectDescription = project.Project_Description__c : "No description input";
                    ftp.Id = project.Id;
                    (project.Quoted_Developer_Hours__c > 0) ? ftp.quoted_developer_hours = project.Quoted_Developer_Hours__c : ftp.quoted_developer_hours = 0;
                    ftp.used_developer_hours = project.Total_Developer_Hours__c;
                    (responseData.test_url !== null) ? ftp.test_url = '<a href="' + responseData.test_url + '" target="_blank">' + responseData.test_url + '</a>'  : ftp.test_url = "Unassigned";
                    (project.Design_Test_Site_URL__c !== null) ? ftp.designSite = project.Design_Test_Site_URL__c : ftp.designSite = "Unassigned";
                    (project.Egnyte_Url__c !== null) ? ftp.Egnyte = project.Egnyte_Url__c : ftp.Egnyte = "Unassigned";
                    (responseData.live_url !== null) ? ftp.live_url = '<a href="' + responseData.live_url + '" target="_blank">' + responseData.live_url + '</a>' : ftp.live_url = "Unassigned";
                    (responseData.ftp_url !== null) ? ftp.ftp_url = responseData.ftp_url : ftp.ftp_url = "Unassigned";
                    (responseData.ftp_host !== null) ? ftp.host = responseData.ftp_host: ftp.host = "Unassigned";
                    (responseData.ftp_user !== null) ? ftp.username = responseData.ftp_user : ftp.username = "Unassigned";
                    (responseData.ftp_password !== null) ? ftp.password = responseData.ftp_password: ftp.password = "Unassigned";
                    (responseData.database_user !== null) ? ftp.database_username = responseData.database_user : ftp.database_username = "Unassigned";
                    (responseData.database_password !== null) ? ftp.database_password = responseData.database_password : ftp.database_password = "Unassigned";
                    (responseData.cart_admin !== null) ? ftp.cart_admin_username = responseData.cart_admin : ftp.cart_admin_username = "Unassigned";
                    (responseData.cart_password !== null) ? ftp.cart_admin_password = responseData.cart_password : ftp.cart_admin_password = "Unassigned";
                    (responseData.cms_admin !== null) ? ftp.cms_admin_username = responseData.cms_admin : ftp.cms_admin_username = "Unassigned";
                    (responseData.cms_password !== null) ? ftp.cms_admin_password = responseData.cms_password : ftp.cms_admin_password = "Unassigned";
                    ftp.domain_notes = responseData.domain_notes;
                    commit('INSERT_FTP_INFO', ftp);
                    commit('TOGGLE_FTP_SHOW');
                    commit('TOGGLE_SPINNER', 'ftp');
                }, function (response) {
                    console.log(response);
                    commit('TOGGLE_SPINNER', 'ftp');
                    Bugsnag.notify("Failed to get project FTP information  |  " + state.currentUser, response.data);
                    switch (response.status) {
                        case 404:
                            commit('ERROR_HANDLER', '404 Error -- Check in with Chris to see what the heck is going on');
                            break;
                        case 401:
                            commit('ERROR_HANDLER', '401 Error -- Looks like your session is expired. Try reloading and trying again.');
                            break;
                        case 403:
                            commit('ERROR_HANDLER', '403 Error -- Request Denied!! Either you\'re an impostor (with no permissions) or we\'ve reached our limit with the API.');
                            break;
                        case 500:
                            commit('ERROR_HANDLER', '500 Error -- An error was encountered with Salesforce.. blame them and try again.');
                            break;
                        default:
                            commit('ERROR_HANDLER', 'Sorry, don\'t recognize this error. Check with Chris to see what\'s going on.');
                    }
                });
        },
        saveProject: function ({commit, state}, details) {
            commit('TOGGLE_SPINNER', 'submit');
            let postData = {
                'time_option': details[0],
                'productionHours': details[1],
                'details': details[2],
                'review': details[3],
                'projectid': details[4]
            };
            let token = document.querySelector('#_token').getAttribute('value');
            let headers = {
                'X-CSRF-TOKEN': token
            };
            Vue.http.post('/api/post/updateproject', postData, headers)
                .then(function (response) {
                    commit('TOGGLE_SPINNER', 'submit');
                    window.location.reload(false);
            }, function (response) {
                window.location.reload(false);
                commit('TOGGLE_SPINNER', 'submit');
                    Bugsnag.notify("Project Submission Error |  " + state.currentUser + " | " +  response.data);
                switch (response.status) {
                    case 404:
                        commit('ERROR_HANDLER', '404 Error -- Check in with Chris to see what the heck is going on');
                        break;
                    case 401:
                        commit('ERROR_HANDLER', '401 Error -- Looks like your session is expired. Try reloading and trying again.');
                        break;
                    case 403:
                        commit('ERROR_HANDLER', '403 Error -- Request Denied!! Either you\'re an impostor (with no permissions) or we\'ve reached our limit with the API.');
                        break;
                    case 500:
                        commit('ERROR_HANDLER', '500 Error -- An error was encountered with Salesforce.. blame them and try again.');
                        break;
                    default:
                        commit('ERROR_HANDLER', 'Sorry, don\'t recognize this error. Check with Chris to see what\'s going on.');
                }
            })
        },
        getTaskFtpInfo: function ({commit, state}, task) {
            commit('TOGGLE_SPINNER', 'ftp');
            commit('SET_ACTIVE_FTP', task.id);
            if (state.ftpShow === true) {
                commit('TOGGLE_FTP_SHOW');
            }
            let postData = {
                'id': task.domainId
            };
            let token = document.querySelector('#_token').getAttribute('value');
            let headers = {
                'X-CSRF-TOKEN': token
            };
            Vue.http.post('/api/post/taskftpinfo', postData, headers)
                .then(function (response) {
                    let ftp = {};
                    commit('ERROR_HANDLER', 'noErr');
                    let responseData = response.data;
                    ftp.name = task.name;
                    ftp.id = task.id;
                    ftp.time = task.time;
                    ftp.notes = task.notes;
                    ftp.type = task.type;
                    ftp.currentTime = task.time;
                    ftp.currentNotes = task.notes;
                    ftp.description = task.description;
                    ftp.liveurl = task.domainURL;
                    ftp.testurl = task.testurl;
                    (task.issuePage !== null) ? ftp.issuePage = task.issuePage : ftp.issuePage = "Unassigned";
                    (task.timequote === 'YES') ? ftp.timeQuote = true : ftp.timeQuote = null;
                    (task.makeLive) ? ftp.makeLive = task.makeLive : ftp.makeLive = 'Unassigned';
                    (responseData.ftp_host !== null) ? ftp.ftp_host = responseData.ftp_host : ftp.ftp_host = "Unassigned";
                    (responseData.ftp_user !== null) ? ftp.ftp_username = responseData.ftp_user : ftp.ftp_username = "Unassigned";
                    (responseData.ftp_password !== null) ? ftp.ftp_password = responseData.ftp_password : ftp.ftp_password = "Unassigned";
                    (responseData.ftp_url !== null) ? ftp.ftp_url = responseData.ftp_url : ftp.ftp_url = "Unassigned";
                    (responseData.database_user !== null) ? ftp.database_username = responseData.database_user : ftp.database_username = "Unassigned";
                    (responseData.database_password !== null) ? ftp.database_password = responseData.database_password : ftp.database_password = "Unassigned";
                    (responseData.cart_admin !== null) ? ftp.cart_admin_username = responseData.cart_admin : ftp.cart_admin_username = "Unassigned";
                    (responseData.cart_password !== null) ? ftp.cart_admin_password = responseData.cart_password : ftp.cart_admin_password = "Unassigned";
                    (responseData.cms_admin !== null) ? ftp.cms_admin_username = responseData.cms_admin : ftp.cms_admin_username = 'Unassigned';
                    (responseData.cms_password !== null) ? ftp.cms_admin_password = responseData.cms_password : ftp.cms_admin_password = "Unassigned";
                    ftp.domain_notes = responseData.domain_notes;
                    commit('INSERT_FTP_INFO', ftp);
                    commit('TOGGLE_FTP_SHOW');
                    commit('TOGGLE_SPINNER', 'ftp');
                }, function (response) {
                    commit('TOGGLE_SPINNER', 'ftp');
                    commit('ERROR_HANDLER', response.data.error);
                    Bugsnag.notify("Failed to get task FTP information  |  " + state.currentUser + " | " +  response.data);
                    switch (response.status) {
                        case 404:
                            commit('ERROR_HANDLER', '404 Error -- Check in with Chris to see what the heck is going on');
                            break;
                        case 401:
                            commit('ERROR_HANDLER', '401 Error -- Looks like your session is expired. Try reloading and trying again.');
                            break;
                        case 403:
                            commit('ERROR_HANDLER', '403 Error -- Request Denied!! Either you\'re an impostor (with no permissions) or we\'ve reached our limit with the API.');
                            break;
                        case 500:
                            commit('ERROR_HANDLER', '500 Error -- An error was encountered with Salesforce.. blame them and try again.');
                            break;
                        default:
                            commit('ERROR_HANDLER', 'Sorry, don\'t recognize this error. Check with Chris to see what\'s going on.');
                    }
                });
        },
        checkConflicting: function ({commit, state}, domainId) {
          commit('EMPTY_CONFLICTING');
          let postData = {
              'id': domainId
          };
          let token = document.querySelector('#_token').getAttribute('value');
          let headers = {
              'X-CSRF-TOKEN': token
          };
          Vue.http.post('/api/post/checkconflicting', postData, headers)
              .then(function (response) {
                  let conflicting = [];
                  let conflictingSeo = JSON.parse(response.data[0]).records;
                  conflictingSeo.forEach(function(e) {
                      if (e.Id !== state.selectedFTP) {
                          conflicting.push({
                              'type': 'SEO Q',
                              'info': {
                                  'id'          : e.Id,
                                  'developer'   : e.Resource__r.Name ? e.Resource__r.Name : 'Unassigned',
                                  'name'        : e.Name ? e.Name : 'Nameless',
                                  'owner'       : e.Owner.Name ? e.Owner.Name : 'Unassigned',
                                  'createdDate' : e.CreatedDate.split('T')[0],
                                  'description' : e.Description__c ? e.Description__c : 'No Description',
                                  'status'      : e.Status__c ? e.Status__c : 'Status Unknown'
                              }
                          })
                      }
                  });
                  let allConflictingProjects = JSON.parse(response.data[1]).records;
                  let conflictingProjectsWithDomain = allConflictingProjects.filter(function(record) {
                      return record.Project_Domains__r;
                  });
                  let conflictingProjects = conflictingProjectsWithDomain.filter(function(record) {
                      return record.Project_Domains__r.records["0"].Domain__c === domainId;
                  });
                  conflictingProjects.forEach(function(e) {
                      conflicting.push({
                          'type': 'Project',
                          'info': {
                              'id'          : e.Id,
                              'developer'   : e.Developer__r.Name ? e.Developer__r.Name : 'Unassigned',
                              'name'        : e.Name ? e.Name : 'Nameless',
                              'owner'       : e.Project_Owner__r.Name ? e.Project_Owner__r.Name : 'Unassigned',
                              'createdDate' : e.CreatedDate.split('T')[0],
                              'description' : e.Project_Description__c ? e.Project_Description__c : 'No Description',
                              'status'      : e.Project_Stage__c ? e.Project_Stage__c : 'Status Unknown'
                          }
                      })
                  });
                  let conflictingDaily = JSON.parse(response.data[2]).records;
                  conflictingDaily.forEach(function(e){
                      if (e.Id !== state.selectedFTP) {
                          conflicting.push({
                              'type': 'Daily Q',
                              'info': {
                                  'id'          : e.Id,
                                  'developer'   : e.Resource__r.Name ? e.Resource__r.Name : 'Unassigned',
                                  'name'        : e.Name ? e.Name : 'Nameless',
                                  'owner'       : e.Owner.Name ? e.Owner.Name : 'Unassigned',
                                  'createdDate' : e.CreatedDate.split('T')[0],
                                  'description' : e.Description__c ? e.Description__c : 'No Description',
                                  'status'      : e.Status__c ? e.Status__c : 'Status Unknown'
                              }
                          })
                      }
                  });
                  commit('INSERT_CONFLICTING',conflicting);
              }, function (response) {
                  Bugsnag.notify("Failed to get conflicting info  |  " + state.currentUser + " | " +  response.data);
              })
        },
        saveTask: function ({commit, state}, details)  {
            commit('TOGGLE_SPINNER', 'submit');
            let postData = {
                'taskHours'     : details[0],
                'details'       : details[1],
                'id'            : details[2],
                'currentTime'   : details[3],
                'currentNotes'  : details[4],
                'objectType'    : details[5],
                'review'        : details[6]
            };

            let token = document.querySelector('#_token').getAttribute('value');
            let headers = {
                'X-CSRF-TOKEN': token
            };
            Vue.http.post('api/post/updatetask', postData, headers)
                .then(function (response) {
                    commit('TOGGLE_SPINNER', 'submit');
                    window.location.reload(false);
            }, function (response) {
                window.location.reload(false);
                commit('ERROR_HANDLER', response.data.error);
                commit('TOGGLE_SPINNER', 'submit');
                Bugsnag.notify("Task Submission Error  |  " + state.currentUser + " | " +  response.data);
                switch (response.status) {
                    case 404:
                        commit('ERROR_HANDLER', '404 Error -- Check in with Chris to see what the heck is going on');
                        break;
                    case 401:
                        commit('ERROR_HANDLER', '401 Error -- Looks like your session is expired. Try reloading and trying again.');
                        break;
                    case 403:
                        commit('ERROR_HANDLER', '403 Error -- Request Denied!! Either you\'re an impostor (with no permissions) or we\'ve reached our limit with the API.');
                        break;
                    case 500:
                        commit('ERROR_HANDLER', '500 Error -- An error was encountered with Salesforce.. blame them and try again.');
                        break;
                    default:
                        commit('ERROR_HANDLER', 'Sorry, don\'t recognize this error. Check with Chris to see what\'s going on.');
                }
            });
        },
        setUser: function ({commit}, user) {
            commit('SET_USER', user);
        },
        conflictingModal: function ({commit}) {
            commit('CONFLICTING_MODAL');
        }
    },
    getters: {
        numberOfConflicts: state => {
            return state.conflicting.length;
        }
    }
})

