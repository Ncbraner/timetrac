import Vue from 'vue'
import ReportApp from './components/parents/ReportApp.vue'

// While in development, comment these out
if (APP_ENV === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug = false;
    Vue.config.silent = true;
}

new Vue({
    el: '#reportMain',
    components: { ReportApp }
});