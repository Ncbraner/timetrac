import Vue          from 'vue'
import ProjectApp   from './components/parents/ProjectApp.vue'
import VueResource  from 'vue-resource'
import store        from './store/store.js'
import wysiwyg      from "vue-wysiwyg";

let clipboard = new Clipboard('.clicky');

Vue.use(wysiwyg, {
    hideModules: {
        "image"         : true,
        "table"         : true,
        "removeFormat"  : true },
});
Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#_token').getAttribute('value');
const http=Vue.http;
export default http

// Set in .env file to make sure devtools are disabled when live
if (APP_ENV === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug    = false;
    Vue.config.silent   = true;
}

new Vue({
    store,
    el: '#projectMain',
    components: { ProjectApp }
});