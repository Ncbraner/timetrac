@extends('main')
@section('content')
	<div class="container home_jumbotron">
		@if (session('status'))
			<div class="alert alert-warning">
				{{ session('status') }}
			</div>
		@endif
	<div class="jumbotron">
		@if( Auth::check())
			<h1>Hello, {{$user->name}}</h1>
            <p><a class="btn btn-lg login-button" href="projects" role="button">View Projects</a> <a class="btn btn-lg login-button" href="tasks" role="button">View Tasks</a></p>
		@else
			<h1>Hello, Please Login!</h1>
			<p><a class="btn btn-lg login-button" href="login" role="button">Login</a></p>
		@endif
	</div>
	</div>
@stop
