@extends('main')
@section('head')
    <meta id="_token" value="{{ csrf_token() }}">
@stop
@section('content')
    <div id="historyMain">
        <history-app></history-app>
    </div>
@stop
@section('vue-bundle')
    <script src="{{URL::asset('js/history-main.js')}}"></script>
@stop