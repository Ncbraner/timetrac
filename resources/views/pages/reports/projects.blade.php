@extends('main')
@section('head')
    <meta id="_token" value="{{ csrf_token() }}">
    @stop
    @section('content')
    <div id="reportMain">
        <report-app></report-app>
    </div>
@stop


@section('vue-bundle')
    <script src="{{ URL::asset('js/report-main.js') }}"></script>
    {{--<script src="{{ URL::asset('js/vue-paginate.js') }}"></script>--}}
@stop