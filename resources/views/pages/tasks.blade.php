@extends('main')
@section('head')
    <meta id="_token" value="{{ csrf_token() }}">
@stop
@section('content')
    <div id="taskMain">
        <task-app></task-app>
    </div>
@stop
@section('vue-bundle')
    <script src="{{URL::asset('js/task-main.js')}}"></script>
@stop