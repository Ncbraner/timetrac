@extends('main')
@section('head')
    <meta id="_token" value="{{ csrf_token() }}">
@stop
@section('content')
    <div id="projectMain">
        <project-app></project-app>
    </div>
@stop
@section('vue-bundle')
    <script src="{{ URL::asset('js/project-main.js') }}"></script>
@stop