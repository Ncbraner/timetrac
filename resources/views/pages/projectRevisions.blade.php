@extends('main')
@section('head')
    <meta id="_token" value="{{ csrf_token() }}">
@stop
@section('content')
    <div id="pageDataWrapper">
        <div v-for="project in projectRevisions | parseProjectRevisions" class="col-md-12">
            <ecw-project-revisions-layout :project="project" ></ecw-project-revisions-layout>
        </div>

    </div>
@stop


@section('vuejs')
    @include('partials.project_revisions_layout')

    <script src="{{ URL::asset('js/ProjectRevisionsLayout.js') }}"></script>
    <script>
        new Vue({
            el: '#pageDataWrapper',
            data: {
                projectRevisions: Ecreativeworks.data
            }
        });

    </script>
@stop