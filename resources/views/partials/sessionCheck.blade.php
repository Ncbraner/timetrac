@if( Auth::check())

    <script>
        function checkSession() {
	    console.log('Checking Session');
            $.post('{{ route('session.ajax.check') }}', { '_token' : '{!! csrf_token() !!}' }, function(data) {
                if (data == 'loggedOut') {
		console.log('123');
                    // User was logged out. Redirect to login page
                     document.location.href = '{!! route('logout') !!}';
                }
                else if (data != '' ) {
                    console.log(data);
                    $( "#dialog-message" ).dialog({
                        modal: true,
                        buttons: {
                            Ok: function() {
                                sessionExtend();
                                $( this ).dialog( "close" );
                            },
                            Logout: function() {
                                document.location.href = '{!! route('logout') !!}';
                            }
                        }
                    });
                }
            });
        }

        function sessionExtend() {
            $.post('{{ route('session.ajax.extend') }}', { '_token' : '{!! csrf_token() !!}' }, function(data) {
                return 'Extended';
            });
        }

        $(function(){
           
            sessionExtend();
            setInterval(checkSession, 5000);
	});

        

        $(document).click(function() {
            sessionExtend();
        });
    </script>

    <div id="dialog-message" title="Session Timeout" style="display:none; width:650px;">
        <p>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
            You are approaching the inactivity timeout. Click ok remain logged in to reset the timer or logout to logout.
        </p>
    </div>
@endif