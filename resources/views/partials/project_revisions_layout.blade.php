<template id="project-revisions-template">
    <div class="info">
        <div class="wrapper">
            <div class="col-md-6">
                <ul class="project_info">
                    <li><label>Project Owner: </label> @{{ project.CreatedBy.Name}}</li>
                    <li><label>Status: </label> @{{ project.Status__c }}</li>
                    <li><label>Created Date: </label> @{{ project.CreatedDate | parseDate }}</li>
                    <li><label>Due Date: </label> @{{ project.Desired_Completion_Date__c }}</li>
                    <li><label>Priority: </label> @{{ project.Priority__c}}</li>
                    <li><label>Revisions should be done on: </label> @{{ project.Make_updates_live_upon_completion__c}}</li>
                    <li><label>Additional Files: </label> @{{ project.Additional_Files_Are_Here__c}}</li>
                    <li><label>Description: </label></li>
                    <li style="list-style: none">@{{ project.Description__c}}</li>
                </ul>
            </div>
            <div class="ftp_container col-md-6" v-show="showFtp">
                <ul class="ftp_info">
                    <li class="header">FTP Info</li>
                    <li class="row_label"><label>Host:</label> @{{ ftp_host }}</li>
                    <li class="ftp_username row_label"><label>Username:</label> @{{ ftp_username }}</li>
                    <li class="ftp_password row_label"><label>Password:</label> @{{ ftp_password }}</li>
                </ul>
                <ul class="ftp_info">
                    <li class="header row_label">Database Info</li>
                    <li class="database_username row_label"><label>Username:</label> @{{ database_username }}</li>
                    <li class="database_password row_label"><label>Password:</label> @{{ database_password }}</li>
                </ul>
                <ul class="ftp_info">
                    <li class="header row_label">Admin Info</li>
                    <li class="cart_admin row_label"><label>Cart Admin Username:</label> @{{ cart_admin_username }}</li>
                    <li class="cart_password row_label"><label>Cart Admin Password:</label> @{{ cart_admin_password }}</li>
                    <li class="cms_admin row_label"><label>CMS Admin Username:</label> @{{ cms_admin_username }}</li>
                    <li class="cms_password row_label"><label>CMS Admin Password:</label> @{{ cms_admin_password }}</li>
                    <li class="header row_label"><label>Notes</label></li>
                    <li class="row_data">@{{ domain_notes }}</li>
                </ul>
            </div>
        </div>
        <div class="ftpSpinner"><i class="fa fa-spinner fa-spin fa-3x" v-show="ftpSpinner"></i></div>
        <button class="getFtpInfo" @click="getFtpInfo" v-show="showGetFtpButton">Show FTP Info</button>
        <button class="hideFtpInfo" @click="hideFtpInfo" v-show="showHideFtpButton">Hide FTP Info</button>
        <button id="show-modal"  @click="addModal">Add Time</button>
        <!-- use the modal component, pass in the prop -->

        <div class="name">
            <p class="data_name">@{{ project.Project__r.Name }} - @{{ project.Name }}</p>
        </div>
    </div>
    <modal>
        <h3 slot="header">@{{ project.Name }}</h3>

        <div slot="body">
            {!! Form::open(['method' => 'post', 'role'  => 'form', 'id'  => 'project_update']) !!}

            <div class="form-group">
                {!! Form::label('productionHours', 'Hours') !!}
                {!! Form::text('productionHours', '', ['class' => 'form-control', 'v-model' => 'productionHours']) !!}
            </div>
            <div class="form-group">
                {!! Form::label('details', 'Details') !!}
                {!! Form::textarea('details', '', ['class' => 'form-control', 'v-model'  =>'details']) !!}
            </div>

            <div class="ftpSpinner"><i class="fa fa-spinner fa-spin fa-3x" v-show="submitSpinner"></i></div>
            <button class="btn btn-danger pull-right" @click="closeModal"> Cancel / Close </button>
            <button class="btn btn-success pull-right" style="margin-right:10px;" @click="saveProject">
            Submit</button>

            {!! Form::close() !!}
        </div>
    </modal>
</template>