
<?php $__env->startSection('head'); ?>
    <meta id="_token" value="<?php echo e(csrf_token()); ?>">
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('content'); ?>
    <div id="reportMain">
        <report-app></report-app>
    </div>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('vue-bundle'); ?>
    <script src="<?php echo e(URL::asset('js/report-main.js')); ?>"></script>
    <?php /*<script src="<?php echo e(URL::asset('js/vue-paginate.js')); ?>"></script>*/ ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>