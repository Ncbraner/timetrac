
<?php $__env->startSection('content'); ?>
	<div class="container home_jumbotron">
		<?php if(session('status')): ?>
			<div class="alert alert-warning">
				<?php echo e(session('status')); ?>

			</div>
		<?php endif; ?>
	<div class="jumbotron">
		<?php if( Auth::check()): ?>
			<h1>Hello, <?php echo e($user->name); ?></h1>
            <p><a class="btn btn-lg login-button" href="projects" role="button">View Projects</a> <a class="btn btn-lg login-button" href="tasks" role="button">View Tasks</a></p>
		<?php else: ?>
			<h1>Hello, Please Login!</h1>
			<p><a class="btn btn-lg login-button" href="login" role="button">Login</a></p>
		<?php endif; ?>
	</div>
	</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>