<nav class="navbar navbar-default" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="/"><img src="<?php echo e(URL::asset('images/logo.png')); ?>" alt=""></a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->

	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<?php if( Auth::check()): ?>
		<ul class="nav navbar-nav">
			<li id="projects"><a href="/projects">Projects</a></li>
			<li id="tasks"><a href="/tasks">Tasks</a></li>
			<li id="reportsprojects"><a href="/reports/projects">Reports</a></li>
			<li id="history"><a href="/history">History</a> </li>
		</ul>
		<?php endif; ?>
		<ul class="nav navbar-nav navbar-right">
			<?php if(Auth::check()): ?>
				<?php if(Auth::user()->role == "admin"): ?>
					<li class="adminHeader"
						data-toggle="tooltip"
						data-placement="bottom"
						title="This is basically a test to make sure user roles are working corectly. If you are seeing this
								you're listed as an admin. In the future you'll start to see more access in the various tabs such as
								reports, what stage your projects are at, etc. For now, the only difference you'll notice is that in
								the history tab, you can search and view each developer/seo/pm's work history, not just your own. Godspeed">
						<span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> Admin Account</li>
				<?php endif; ?>
				<li><a href="#" class="refresh-projects-tasks"><i class="fa fa-refresh"></i> Refresh Projects & Tasks</a></li>
				<li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>
			<?php else: ?>
				<li><a href="/login"><i class="fa fa-sign-in"></i> Login</a></li>
			<?php endif; ?>
		</ul>
	</div><!-- /.navbar-collapse -->
</nav>