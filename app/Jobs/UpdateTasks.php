<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Session;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Ecreativeworks\Salesforce\Api\Get\Task;
use Ecreativeworks\Salesforce\Api\Get\Projects;
use App\Http\Controllers\SalesforceApi;
use GuzzleHttp\Client;

class UpdateTasks extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SalesforceApi $api)
    {
        $token = Session::get('token');
        $client = new Client();
        $projects = new Projects($client);
        $api->getProjects($projects);

    }
}
