<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesforceResource extends Model
{
  protected $fillable	= ['user_id', 'resource_id', 'resource_name'];
  protected $table		= 'SalesforceResources';


  /**
   * A resource is a user
   * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
   */
  function getUser(){
	return $this->belongsTo('App/User');
  }

}
