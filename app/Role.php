<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['role_id', 'role_name'];
    protected $table = 'user_roles';
}
