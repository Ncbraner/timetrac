<?php

namespace App\Console\Commands;

use Session;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\SalesforceApi;
use Ecreativeworks\Salesforce\Api\Get\TimeOptions;



class update extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:tasks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $token = Cache::get('token');
        Session::put('token', $token);
        $api = new SalesforceApi();
        $api->getTasks();
        $api->getProjects();
        $api->getTime(new TimeOptions());
    }
}
