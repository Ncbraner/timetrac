<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'salesforce_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A user has one resource, which is themself
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function salesforceResource()
    {
        return $this->hasOne('App\SalesforceResource');
    }

    public static function getAdminUser()
    {
        return self::where('email', env('ADMIN_USER_EMAIL'))->first();
    }

}
