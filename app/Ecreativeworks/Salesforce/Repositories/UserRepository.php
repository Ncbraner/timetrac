<?php

namespace Ecreativeworks\Salesforce\Repositories;

use App\User;
use App\SalesforceResource;

class UserRepository{

	public function saveUser($salesforceUser){
	  $user = User::firstOrCreate(array(
		'name'			=> $salesforceUser->name,
		'email'			=> $salesforceUser->email,
		'salesforce_id'	=> $salesforceUser->id,
	  ));
	  return $user;

	}

	public function saveUserRole(){

	}

}