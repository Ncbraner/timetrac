<?php

namespace Ecreativeworks\Salesforce\Repositories;

use Auth;
use App\SalesforceResource;

class ResourceRepository {


  public function createResource($resource, $user) {
	  $resource = $this->findOrCreate(
		$resource, $user->id
	  );
  }

  public function findOrCreate($resource, $user_id)
  {
	$result = SalesforceResource::firstOrCreate(array(
	  'resource_id'			=> $resource['id'],
	  'resource_name'		=> $resource['name'],
	  'user_id'				=> $user_id
	));

  }

	public function getResource(){
		$resource = Auth::user()->salesforceResource;
		$resource = json_decode($resource);
		return $resource->resource_id;
	}

}