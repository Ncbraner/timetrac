<?php

namespace Ecreativeworks\Salesforce;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Redirect;
use Ecreativeworks\Salesforce\Api\Get\UserRole;
use Ecreativeworks\Salesforce\Api\Get\Projects;
use Ecreativeworks\Salesforce\Api\Get\Resources;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use Ecreativeworks\Salesforce\Repositories\UserRepository;

class SalesforceAuthenticationHandler
{


    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * @var \Ecreativeworks\Salesforce\Repositories\UserRepository
     */
    private $userRepository;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\Resources
     */
    private $resources;
    /**
     * @var \Ecreativeworks\Salesforce\Api\SalesforceConnector
     */
    private $connector;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\UserRole
     */
    private $role;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\Projects
     */
    private $projects;

    public function __construct(Request $request, UserRepository $userRepository, Resources $resources, UserRole $role, SalesforceConnector $connector, Projects $projects)
    {
        $this->request = $request;
        $this->userRepository = $userRepository;
        $this->resources = $resources;
        $this->connector = $connector;
        $this->role = $role;
        $this->projects = $projects;
    }


    public function doLogin($path)
    {
        //TODO this is a mess, needs refactored, does way to much
        if ($this->request->input('code')) {
            $user = $this->getSalesforceUser();
            $appUser = $this->saveUser($user);
            $this->logUserIn($appUser);
            $this->saveToken($user->accessTokenResponseBody['refresh_token'], $appUser);
            $this->resources->getResources();
            $appUserRole = $this->role->getRoleForUser($appUser);
            $this->projects->getProjects();

            return Redirect::to($path);
        }
        return $this->salesforceLoginRedirect();
    }

    /**
     * @param $appUser
     */
    public function logUserIn($appUser)
    {
        Auth::login($appUser, TRUE);
    }

    /**
     * @param $user
     *
     * @return static
     */
    public function saveUser($user)
    {
        $appUser = $this->userRepository->saveUser($user);
        return $appUser;
    }

    public function saveToken($token, $user)
    {
        Session::put('token', $token);
        Cache::put('token', $token, (60 * 1000)); //some random number to keep token in cache so cron jobs can use it
        //save token to user now, it never expires in salesforce, so keeping it on the user table and updating it every
        //time they login, probably fix the issue
        $user->refresh_token = $token;
        $user->save();
    }

    /**
     * @return mixed
     */
    public function getSalesforceUser()
    {
        $user = Socialite::with('salesforce')->user();
        return $user;
    }

    /**
     * @return mixed
     */
    public function salesforceLoginRedirect()
    {
        return Socialite::with('salesforce')->redirect();
    }

    public function logout()
    {
        $this->flushSessions();
        $this->logUserOut();

    }

    protected function logUserOut()
    {
        Auth::logout();
    }

    protected function flushSessions()
    {
        return Session::flush();
    }

}