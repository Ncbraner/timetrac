<?php

namespace Ecreativeworks\Salesforce\Library;

use App\Options;
use App\Records;
use App\User;
use Auth;
use Carbon\Carbon;
use Ecreativeworks\Salesforce\Repositories\RecordsRepository;
use function GuzzleHttp\Psr7\_caseless_remove;
use Illuminate\Support\Facades\Cache;
use DB;


class PrepareDataForView
{

    protected $id;
    protected $returnData;
    protected $salesforce_id;

    public function getUserId()
    {
        $this->id = Auth::user()->id;
    }

    public function getSalesforceResourceId()
    {
        $this->salesforce_id = User::find($this->id)->salesforceResource->resource_id;
    }

    public function prepareLink($url)
    {
        if (strpos($url,'http') !== false) {
            return $url;
        } elseif (strpos($url,'www') === 0) {
            return 'http://' . $url;
        } elseif (strpos($url, 'www') === false) {
            return 'http://www.' . $url;
        } else {
            return 'Not set or not a valid url ¯\_(ツ)_/¯';
        }
    }

    public function returnUserData($dataType)
    {
        $records = json_decode(Cache::get($dataType));
        $returnData = [];
        $this->returnData['salesforce_id'] = $this->salesforce_id;

        if (!$records) {
            $this->returnData['data'] = [];
        } else {
            foreach ($records as $record) {
                $recordDeveloperId = (isset($record->Developer__r->Id) ? $record->Developer__r->Id : '');
                if ($recordDeveloperId == $this->salesforce_id) {
                    $returnData[] = $record;
                }
            }
            $this->returnData['data'] = $returnData;
        }
    }

    public function returnProjectReportData($dataType, Carbon $carbon){

        $data = json_decode(Cache::get($dataType));

        if ($data == null) {
          return null;
        }

        $return_data = [];
        $i=0;
        foreach($data as $row){

            if($row->Project_Stage__c != "In Development"){
                continue;
            }
            $resource = (isset($row->Developer__r->Name) ? $row->Developer__r->Name : 'Unassigned');
            $owner = (isset($row->Project_Owner__r->Name) ? $row->Project_Owner__r->Name : '');
            $createdDate  = substr($row->CreatedDate, 0, strpos($row->CreatedDate, "T"));

            $return_data[$resource][$i]['id']                      = $row->Id;
            $return_data[$resource][$i]['account']                 = $row->Name;
            $return_data[$resource][$i]['name']                    = $row->Name;
            $return_data[$resource][$i]['createdDate']             = $createdDate;
            $return_data[$resource][$i]['priority']                = '';
            $return_data[$resource][$i]['type']                    = $row->RecordType->Name;
            $return_data[$resource][$i]['owner']                   = $owner;
            $return_data[$resource][$i]['stage']                   = $row->Project_Stage__c;
            $return_data[$resource][$i]['quoted_hours']            = $row->Quoted_Production_Hours_Rollup__c;
            $return_data[$resource][$i]['due_date']                = $row->Buildout_Due_Date__c;
            $return_data[$resource][$i]['developer']               = $resource;
            $i++;
        }

        //force tasks into the list as well
        $tasks = $this->tasks($carbon);
        foreach($tasks as $row){
            if($row['status'] == "In Review" || $row['status'] == "On Hold"){
                continue;
            }
            $resource = (isset($row['resourceName']) ? $row['resourceName'] : 'Unassigned');

            $dueDate  = (isset($row['dueDate']) && $row['dueDate'] !=='' ? $row['dueDate'] : '');

            $return_data[$resource][$i]['id']                      = $row['id'];
            $return_data[$resource][$i]['account']                 = $row['account'];
            $return_data[$resource][$i]['createdDate']             = $row['createdDate'];
            $return_data[$resource][$i]['priority']                = $row['priority'];
            $return_data[$resource][$i]['name']                    = $row['name'];
            $return_data[$resource][$i]['type']                    = $row['typeLong'];
            $return_data[$resource][$i]['owner']                   = $row['owner'];
            $return_data[$resource][$i]['stage']                   = $row['status'];
            $return_data[$resource][$i]['quoted_hours']            = '';
            $return_data[$resource][$i]['due_date']                = $row['dueDate'];
            $return_data[$resource][$i]['developer']               = $resource;
            $return_data[$resource][$i]['pastDue']                 = $row['pastDue'];
            $i++;
        }

        $sortkey = array();
        foreach($return_data as $key => $value){
            $sortkey[] = $key;
        }
        array_multisort($sortkey, SORT_ASC, $return_data);
        return $return_data;
    }


    public function getTimeOptions($dataType)
    {
        $timeOptions = '';
        $records     = Options::where('key', 'timeOptions')->first();
        $records     = json_decode($records->value);
        switch ($dataType) {
            case 'projects':
                $timeOptions = $records->Type_of_Production_Task__c;
                break;
            case 'seo':
                $timeOptions = $records->Type_of_SEO_Task__c;
                break;
        }
        $this->returnData['timeOptions'] = $timeOptions;
    }

    public function tasks(Carbon $carbon)
    {
        //get all the info for tasks that are not projects
        $projectRevisions = Cache::get('projectrevisions');
        $dailyqs          = Cache::get('dailyqs');
        $seoqs            = Cache::get('seoqs');
        $designRevisions  = Cache::get('designrevisions');

        $returnTasks = [];

        $i = 0;
        if($projectRevisions) {
            foreach ($projectRevisions as $pr) {
                $resource = (isset($pr->Developer__r->Id) ? $pr->Developer__r->Id : 'Unassigned');
                $resourceName = (isset($pr->Developer__r->Name) ? $pr->Developer__r->Name : 'Unassigned');
                $domainId = (isset($pr->Domain__c) ? $pr->Domain__c : 'Unassigned ¯\_(ツ)_/¯');
                $domainURL = (isset($pr->Domain__r->Domain_Name__c) ? '<a href="' . $this->prepareLink($pr->Domain__r->Domain_Name__c) . '" target="_blank">' . $pr->Domain__r->Domain_Name__c . '</a>' : 'Not set or not a valid url ¯\_(ツ)_/¯');
                $notes = (isset($pr->Notes__c) ? $pr->Notes__c : '');
                $time = (isset($pr->Time_Hrs__c) ? $pr->Time_Hrs__c : 0);
                $testUrl = (isset($pr->Domain__r->Test_Site_Url__c) ? '<a href="' . $this->prepareLink($pr->Domain__r->Test_Site_Url__c) . '" target="_blank">' . $pr->Domain__r->Test_Site_Url__c . '</a>' : 'Unassigned ¯\_(ツ)_/¯');
                $createdDate = substr($pr->CreatedDate, 0, strpos($pr->CreatedDate, "T"));
                $date = (!is_null($pr->Desired_Completion_Date__c) && $pr->Desired_Completion_Date__c != '') ? $pr->Desired_Completion_Date__c : DueDate::setDueDate($pr->Priority__c, $createdDate);
                $pastDue = (strtotime($date) < strtotime(Carbon::now('America/Chicago')) ? true : false);

                $returnTasks[$i]['id'] = $pr->Id;
                $returnTasks[$i]['account'] = $pr->Project__r->Name;
                $returnTasks[$i]['createdDate'] = $createdDate;
                $returnTasks[$i]['priority'] = $pr->Priority__c;
                $returnTasks[$i]['projectId'] = $pr->Project__r->Id;
                $returnTasks[$i]['name'] = $pr->Name;
                $returnTasks[$i]['owner'] = $pr->CreatedBy->Name;
                $returnTasks[$i]['domain'] = $pr->Project__r->Name;
                $returnTasks[$i]['testurl'] = $testUrl;
                $returnTasks[$i]['domainId'] = $domainId;
                $returnTasks[$i]['domainURL'] = $domainURL;
                $returnTasks[$i]['status'] = $pr->Status__c;
                $returnTasks[$i]['dueDate'] = $date;
                $returnTasks[$i]['description'] = $pr->Description__c;
                $returnTasks[$i]['resource'] = $resource;
                $returnTasks[$i]['resourceName'] = $resourceName;
                $returnTasks[$i]['notes'] = $notes;
                $returnTasks[$i]['time'] = $time;
                $returnTasks[$i]['type'] = 'pr';
                $returnTasks[$i]['typeLong'] = 'Project Revision';
                $returnTasks[$i]['pastDue'] = $pastDue;

                $i++;
            }
        }
        if($designRevisions) {
            foreach ($designRevisions as $dr) {
                $resource = (isset($dr->Developer__r->Id) ? $dr->Developer__r->Id : 'Unassigned');
                $resourceName = (isset($dr->Developer__r->Name) ? $dr->Developer__r->Name : 'Unassigned');
                $domainId = (isset($dr->Domain__c) ? $dr->Domain__c : 'Unassigned');
                $domainURL = (isset($dr->Domain__r->Domain_Name__c) ? '<a href="' . $this->prepareLink($dr->Domain__r->Domain_Name__c) . '" target="_blank">' . $dr->Domain__r->Domain_Name__c . '</a>' : 'Not set or not a valid url ¯\_(ツ)_/¯');
                $notes = (isset($dr->Notes__c) ? $dr->Notes__c : '');
                $time = (isset($dr->Time_Hrs__c) ? $dr->Time_Hrs__c : 0);
                $testUrl = (isset($dr->Domain__r->Test_Site_Url__c) ? '<a href="' . $dr->Domain__r->Test_Site_Url__c . '" target="_blank">' . $dr->Domain__r->Test_Site_Url__c . '</a>' : 'Unassigned ¯\_(ツ)_/¯');
                $createdDate = substr($dr->CreatedDate, 0, strpos($dr->CreatedDate, "T"));
                $date = (!is_null($dr->Desired_Completion_Date__c) && $dr->Desired_Completion_Date__c != '') ? $dr->Desired_Completion_Date__c : DueDate::setDueDate($pr->Priority__c, $createdDate);
                $pastDue = (strtotime($date) < strtotime(Carbon::now('America/Chicago')) ? true : false);


                $returnTasks[$i]['id'] = $dr->Id;
                $returnTasks[$i]['account'] = $dr->Project__r->Name;
                $returnTasks[$i]['createdDate'] = $createdDate;
                $returnTasks[$i]['priority'] = $dr->Priority__c;
                $returnTasks[$i]['projectId'] = $dr->Project__r->Id;
                $returnTasks[$i]['name'] = $dr->Name;
                $returnTasks[$i]['owner'] = $dr->CreatedBy->Name;
                $returnTasks[$i]['domain'] = $dr->Project__r->Name;
                $returnTasks[$i]['testurl'] = $testUrl;
                $returnTasks[$i]['domainId'] = $domainId;
                $returnTasks[$i]['domainURL'] = $domainURL;
                $returnTasks[$i]['status'] = $dr->Status__c;
                $returnTasks[$i]['dueDate'] = $date;
                $returnTasks[$i]['description'] = $dr->Description__c;
                $returnTasks[$i]['resource'] = $resource;
                $returnTasks[$i]['resourceName'] = $resourceName;
                $returnTasks[$i]['notes'] = $notes;
                $returnTasks[$i]['time'] = $time;
                $returnTasks[$i]['type'] = 'dr';
                $returnTasks[$i]['typeLong'] = 'Design Revision';
                $returnTasks[$i]['pastDue'] = $pastDue;

                $i++;
            }
        }
        if($dailyqs) {
            foreach ($dailyqs as $dq) {

                $resource = (isset($dq->Resource__r->Id) ? $dq->Resource__r->Id : 'Unassigned');
                $resourceName = (isset($dq->Resource__r->Name) ? $dq->Resource__r->Name : 'Unassigned');
                $domainName = (isset($dq->Domain__r->Name) ? $dq->Domain__r->Name : 'Unassigned');
                $domainId = (isset($dq->Domain__r->Id) ? $dq->Domain__r->Id : 'Unassigned');
                $domainURL = (isset($dq->Domain__r->Domain_Name__c) ? '<a href="' . $this->prepareLink($dq->Domain__r->Domain_Name__c) . '" target="_blank">' .$dq->Domain__r->Domain_Name__c . '</a>' : 'Not set or not a valid url ¯\_(ツ)_/¯');
                $notes = (isset($dq->Notes__c) ? $dq->Notes__c : '');
                $time = (isset($dq->Actual_Time__c) ? $dq->Actual_Time__c : 0);
                $testUrl = (isset($dq->Domain__r->Test_Site_Url__c) ? '<a href="' . $this->prepareLink($dq->Domain__r->Test_Site_Url__c) . '" target="_blank">' . $dq->Domain__r->Test_Site_Url__c . '</a>' : 'Unassigned ¯\_(ツ)_/¯');
                $createdDate = substr($dq->CreatedDate, 0, strpos($dq->CreatedDate, "T"));
                $date = (!is_null($dq->Desired_Completion_Date__c) && $dq->Desired_Completion_Date__c != '') ? $dq->Desired_Completion_Date__c : DueDate::setDueDate($dq->Priority__c, $createdDate);
                $pastDue = (strtotime($date) < strtotime(Carbon::now('America/Chicago')) ? true : false);
                $makeLive = (isset($dq->Make_updates_live_upon_completion__c) ? $dq->Make_updates_live_upon_completion__c : null);
                $issuePage = (isset($dq->Link_to_Page_with_Issue__c) ? $dq->Link_to_Page_with_Issue__c : null);


                $returnTasks[$i]['id']              = $dq->Id;
                $returnTasks[$i]['account']         = $dq->Account__r->Name;
                $returnTasks[$i]['createdDate']     = $createdDate;
                $returnTasks[$i]['priority']        = $dq->Priority__c;
                $returnTasks[$i]['name']            = $dq->Name;
                $returnTasks[$i]['owner']           = $dq->Owner->Name;
                $returnTasks[$i]['domain']          = $domainName;
                $returnTasks[$i]['domainId']        = $domainId;
                $returnTasks[$i]['domainURL']       = $domainURL;
                $returnTasks[$i]['testurl']         = $testUrl;
                $returnTasks[$i]['status']          = $dq->Status__c;
                $returnTasks[$i]['timequote']       = $dq->Time_Quote_Only_Needed__c;
                $returnTasks[$i]['dueDate']         = $date;
                $returnTasks[$i]['description']     = $dq->Description__c;
                $returnTasks[$i]['resource']        = $resource;
                $returnTasks[$i]['resourceName']    = $resourceName;
                $returnTasks[$i]['notes']           = $notes;
                $returnTasks[$i]['time']            = $time;
                $returnTasks[$i]['type']            = 'dq';
                $returnTasks[$i]['typeLong']        = 'Daily Q';
                $returnTasks[$i]['pastDue']         = $pastDue;
                $returnTasks[$i]['makeLive']        = $makeLive;
                $returnTasks[$i]['issuePage']       = $issuePage;
                $i++;
            }
        }

        if($seoqs) {
            foreach ($seoqs as $seoq) {
                $resource = (isset($seoq->Resource__r->Id) ? $seoq->Resource__r->Id : 'Unassigned');
                $resourceName = (isset($seoq->Resource__r->Name) ? $seoq->Resource__r->Name : 'Unassigned');
                $domainName = (isset($seoq->Domain__r->Name) ? $seoq->Domain__r->Name : 'Unassigned');
                $domainId = (isset($seoq->Domain__r->Id) ? $seoq->Domain__r->Id : 'Unassigned');
                $domainURL = (isset($seoq->Domain__r->Domain_Name__c) ? '<a href="' . $this->prepareLink($seoq->Domain__r->Domain_Name__c) . '" target="_blank">' . $seoq->Domain__r->Domain_Name__c . '</a>' : 'Not set or not a valid url ¯\_(ツ)_/¯');
                $notes = (isset($seoq->Notes__c) ? $seoq->Notes__c : '');
                $time = (isset($seoq->Time__c) ? $seoq->Time__c : 0);
                $testUrl = (isset($seoq->Domain__r->Test_Site_Url__c) ? '<a href="' . $seoq->Domain__r->Test_Site_Url__c . '" target="_blank">' . $seoq->Domain__r->Test_Site_Url__c . '</a>' : 'Unassigned ¯\_(ツ)_/¯');
                $createdDate = substr($seoq->CreatedDate, 0, strpos($seoq->CreatedDate, "T"));
                $date = (!is_null($seoq->Desired_Completion_Date__c) && $seoq->Desired_Completion_Date__c != '') ? $seoq->Desired_Completion_Date__c : DueDate::setDueDate($seoq->Priority__c, $createdDate);
                $pastDue = (strtotime($date) < strtotime(Carbon::now('America/Chicago')) ? true : false);
                $makeLive = (isset($seoq->Make_updates_live_upon_completion__c) ? $seoq->Make_updates_live_upon_completion__c : null);

                $returnTasks[$i]['id']            = $seoq->Id;
                $returnTasks[$i]['account']       = $seoq->Account__r->Name;
                $returnTasks[$i]['createdDate']   = $createdDate;
                $returnTasks[$i]['priority']      = $seoq->Priority__c;
                $returnTasks[$i]['name']          = $seoq->Name;
                $returnTasks[$i]['owner']         = $seoq->Owner->Name;
                $returnTasks[$i]['domain']        = $domainName;
                $returnTasks[$i]['domainId']      = $domainId;
                $returnTasks[$i]['domainURL']     = $domainURL;
                $returnTasks[$i]['testurl']       = $testUrl;
                $returnTasks[$i]['status']        = $seoq->Status__c;
                $returnTasks[$i]['timequote']     = $seoq->Time_Quote_Only_Needed__c;
                $returnTasks[$i]['dueDate']       = $date;
                $returnTasks[$i]['description']   = $seoq->Description__c;
                $returnTasks[$i]['resource']      = $resource;
                $returnTasks[$i]['resourceName']  = $resourceName;
                $returnTasks[$i]['notes']         = $notes;
                $returnTasks[$i]['time']          = $time;
                $returnTasks[$i]['type']          = 'seoq';
                $returnTasks[$i]['typeLong']      = 'Seo Q';
                $returnTasks[$i]['pastDue']       = $pastDue;
                $returnTasks[$i]['makeLive']      = $makeLive;

                $i++;
            }
        }
        return $this->prioritizeBackToDeveloper($returnTasks);
    }


    public function execute($dataType)
    {
        switch ($dataType) {
            case 'projects':
                $this->getUserId();
                $this->getSalesforceResourceId();
                $this->returnUserData($dataType);
                $projects = $this->returnData['data'];
                if ($projects) {
                    foreach ($projects as $p) {
                        $description = (isset($p->Project_Description__c) ? $p->Project_Description__c : 'This projection description is a newer thing that was added to Salesforce. If you\'re seeing this and not a description, ask the PM to put it in salesforce under the project scope field. Hopefully by doing this, it will become habit and all of the descriptions will come through here');
                        $designTestURL = (isset($p->Test_Site_URL__c) ? '<a href="' . $this->prepareLink($p->Test_Site_URL__c) . '" target="_blank">' . $p->Test_Site_URL__c . '</a>'  : 'Unassigned ¯\_(ツ)_/¯');
                        $liveURL = (isset($p->Production_Test_Site_URL__c) ? '<a href="' . $this->prepareLink($p->Production_Test_Site_URL__c) . '" target="_blank">' . $p->Production_Test_Site_URL__c . '</a>' : 'Unassigned ¯\_(ツ)_/¯');

                        $p->Project_Description__c      = $description;
                        $p->Design_Test_Site_URL__c     = $designTestURL;
                        $p->Production_Test_Site_URL__c = $liveURL;
                    }
                }
                $this->getTimeOptions($dataType);
                return $this->returnData;
                break;
            case 'history':
                $this->returnData = DB::select('select resource_name, id from SalesforceResources order by resource_name');
                return $this->returnData;
                break;
            default:
                $this->getUserId();
                $this->getSalesforceResourceId();
                $this->returnUserData($dataType);
                $this->getTimeOptions($dataType);
                return $this->returnData;
        }
    }

    public function prioritizeBackToDeveloper($allTasks){
        $priorityTasks = [];
        $otherTasks = [];
        $i = 0;
        foreach($allTasks as $index => $task){
            foreach($task as $key => $value){
                if($task['priority'] == 'Emergency'){
                    $priorityTasks[$i] = $task;
                }else{
                    $otherTasks[$i] = $task;
                }
            }
            $i++;
        }
        $priorityTasks  = $this->sortTasks($priorityTasks);
        $otherTasks     = $this->sortTasks($otherTasks);
        return array_merge($priorityTasks, $otherTasks);
    }

    public function sortTasks($tasks){
        //sort them based on due date
        usort($tasks, function($a, $b) {
            return $a['dueDate'] <=> $b['dueDate'];
        });
        return $tasks;
    }

}
