<?php


namespace Ecreativeworks\Salesforce\Library;


class DueDate
{

    public static function setDueDate( $priority, $datesubmited ) {
        $datesubmited = substr( $datesubmited, 0, 10 );
        $datesubmited = date_create( $datesubmited );
        switch ( $priority ) {
            case 'Low':
                $duedate = date_add( $datesubmited, date_interval_create_from_date_string( "7 days" ) );
                break;
            case 'Standard':
                $duedate = date_add( $datesubmited, date_interval_create_from_date_string( "5 days" ) );
                break;
            case 'High':
                $duedate = date_add( $datesubmited, date_interval_create_from_date_string( "3 days" ) );
                break;
            case 'Emergency':
                $duedate = date_add( $datesubmited, date_interval_create_from_date_string( "1 days" ) );
                break;
        }
        $duedate = $duedate->format( 'Y-m-d' );

        return $duedate;

    }

}