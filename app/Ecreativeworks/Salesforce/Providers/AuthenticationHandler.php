<?php

namespace Ecreativeworks\Salesforce\Providers;

use App;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Ecreativeworks\Salesforce\Api\Get\Resources;
use Ecreativeworks\Salesforce\Repositories\UserRepository;
use Ecreativeworks\Salesforce\SalesforceAuthenticationHandler;


class AuthenticationHandler extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	  App::bind( 'SalesforceAuthenticationHandler', function(){
		$request = $this->app->make(Request::class);
		$userRepository = $this->app->make(UserRepository::class);
		$resource = $this->app->make(Resources::class);
		return new SalesforceAuthenticationHandler($request, $userRepository, $resource);
	  });
    }
}
