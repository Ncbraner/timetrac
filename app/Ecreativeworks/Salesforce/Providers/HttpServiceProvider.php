<?php

namespace Ecreativeworks\Salesforce\Providers;

use App;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\HttpFoundation\Request;
use Ecreativeworks\Salesforce\Repositories\UserRepository;
use Ecreativeworks\Salesforce\SalesforceAuthenticationHandler;
use Ecreativeworks\Salesforce\Api\Get\Projects;


class HttpServiceProvider extends ServiceProvider
{
  /**
   * Bootstrap the application services.
   *
   * @return void
   */
  public function boot()
  {
	//
  }

  /**
   * Register the application services.
   *
   * @return void
   */
  public function register()
  {
	App::bind( 'HttpClient', function($app){
	  return new Client();
	});
  }
}
