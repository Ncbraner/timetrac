<?php

namespace Ecreativeworks\Salesforce\Api;

use Log;
use Session;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Exception\ClientException;

class SalesforceConnector{

  /**
   * @var \GuzzleHttp\Client
   */
  private $client;

  private $url;

  private $token;

  public function __construct() {
	$this->getInstanceUrl();
	$this->client = app('HttpClient');
  }

  protected function getInstanceUrl(){
	$this->url = env('SALESFORCE_INSTANCE_URL');
  }

  public function setHeaders() {
	$headers = [
	  'Authorization'	=> 'Bearer '.$this->token,
	  'Content-type'	=> 'application/json'
	];
	return $headers;
  }

  public function getSoqlQueryUrl($queryString){
	return $this->url.'/services/data/v32.0/query/?q='.$queryString;
  }

  protected function fixQueryString($query){
	return str_replace(array("\r", "\n", "\t"), ' ', $query);
  }

  public function parseResponse($response){
    if(!$response){
        return;
    }
	$records = $response->records;
	$returnRecords = [];
	foreach($records as $record){
	  unset($record->attributes);
	  $returnRecords[] = $record;
	}
	return json_encode($returnRecords);
  }

  public function storeRecords($type, $records){
	  Cache::put($type, json_decode($records), 86400);
  }

  public function sendRequest($query){
	  $token = $this->getAdminToken();
	  
	  $query = $this->fixQueryString($query);
	try {

	  $res = $this->client->request('GET', $this->getSoqlQueryUrl($query), [
		'headers' => [
		  'Authorization' => 'Bearer ' . $token,
		  'Content-type'  => 'application/json'
		],
	  ])->getBody()->getContents();
	  return $res;
	}catch(ClientException $e){
	  //TODO write exception class that makes sense to see if they were logged out of salesfoce
	  return json_encode([
		'error' => $e->getMessage(),
		'code'  => $e->getCode()
	  ]);

	}
  }

  public function getObjectFields($object, $id){
	try{
	  $url = $this->url."/services/data/v32.0/sobjects/{$object}/{$id}/?fields";
	  $token = $this->getNewToken();
	  $response = $this->client->request('GET', $url, [
		'headers' => [
		  'Authorization' => 'Bearer ' . $token,
		  'Content-type'  => 'application/json'
		],
	  ])->getBody()->getContents();
	  return $response;
	}catch(ClientException $e){
		//
	}
  }

  public function describeObject($objectName){
  	$uri = "/services/data/v32.0/sobjects/{$objectName}/describe/";
	$url = $this->url . $uri;
	$token = $this->getNewToken();
	try {
	  $res = $this->client->request('GET', $url, [
		'headers' => [
		  'Authorization' => 'Bearer ' . $token,
		  'Content-type'  => 'application/json'
		],
	  ])->getBody()->getContents();
	  return $res;
	}catch(ClientException $e){
	  //
	}
  }

  public function saveObject($objectName,  $data){
	$uri = "/services/data/v32.0/sobjects/{$objectName}";
	$url = $this->url . $uri;
	$token = $this->getNewToken();

	try {
	  $res = $this->client->request('POST', $url, [
		'headers' => [
		  'Authorization' => 'Bearer ' . $token,
		  'Content-type'  => 'application/json'
		],
		'json' => $data,
	  ])->getBody()->getContents();
	  return  $res;

	}catch(ClientException $e){
	  $returnArray = [
	      'error'   => true,
        'status'  => $e->getCode(),
        'data'    => $e->getMessage()
      ];
	  return json_encode($returnArray);
	}catch(\Exception $e){
        $returnArray = [
            'error'   => true,
            'status'  => $e->getCode(),
            'data'    => $e->getMessage()
        ];
        return json_encode($returnArray);
    }
  }

	public function updateObject($objectName, $ObjectId,  $data){
		$uri = "/services/data/v32.0/sobjects/{$objectName}/$ObjectId";
		$url = $this->url . $uri;
		$token = $this->getNewToken();
		
		try {
			$res = $this->client->request('PATCH', $url, [
					'headers' => [
							'Authorization' => 'Bearer ' . $token,
							'Content-type'  => 'application/json'
					],
					'json' => $data,
			])->getBody()->getContents();
			return  $res;

		}catch(ClientException $e){
            $returnArray = [
                'error'   => true,
                'status'  => $e->getCode(),
                'data'    => $e->getMessage()
            ];
            return json_encode($returnArray);
		}catch(\Exception $e){
            $returnArray = [
                'error'   => true,
                'status'  => $e->getCode(),
                'data'    => $e->getMessage()
            ];
            return json_encode($returnArray);
        }
	}


  public function getNewToken(){
	    $user   = Auth::user();
	    $token  = $user->refresh_token;
	    $url    = 'https://login.salesforce.com/services/oauth2/token';
	    $res    = $this->client->request('POST', $url, [
	                'headers'       => [ 'Content-type' => 'application/x-www-form-urlencoded' ],
	                'form_params'   => [ 'grant_type'	  => 'refresh_token',
                  'client_id'		  => env('SALESFORCE_KEY'),
		              'client_secret'	=> env('SALESFORCE_SECRET'),
		              'refresh_token'	=> $token ],
      ])->getBody()->getContents();

	$response = json_decode($res);
	return $response->access_token;
  }

    public function getAdminToken(){
        $user = User::getAdminUser();
        $token = $user->refresh_token;
        $url = 'https://login.salesforce.com/services/oauth2/token';
        $res = $this->client->request('POST', $url, [
                  'headers'       =>  [ 'Content-type'  => 'application/x-www-form-urlencoded' ],
                  'form_params'   =>  [ 'grant_type'    => 'refresh_token',
                  'client_id'		  => env('SALESFORCE_KEY'),
                  'client_secret'	=> env('SALESFORCE_SECRET'),
                  'refresh_token'	=> $token ],
        ])->getBody()->getContents();

        $response = json_decode($res);
        return $response->access_token;
    }
}
