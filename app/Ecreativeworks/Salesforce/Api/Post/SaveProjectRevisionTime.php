<?php

namespace Ecreativeworks\Salesforce\Api\Post;

use App\SalesforceResource;
use Auth;
use Carbon\Carbon;
use Ecreativeworks\Salesforce\Api\Get\ProjectRevisions;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Request;

class SaveProjectRevisionTime extends SalesforceConnector
{

    /**
     * @var \Request
     */
    private $request;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\Projects
     */
    private $projectRevisions;

    public function __construct(
      Client $client,
      Request $request,
      ProjectRevisions $projectRevisions
    ) {
        parent::__construct($client);
        $this->request  = $request;
        $this->projectRevisions = $projectRevisions;
    }

    public function saveProjectRevision()
    {

        if (!Auth::check()) {
            return 'Login Fool!';
        }
        $user        = Auth::user();
        $id          = $user->id;
        $resource    = SalesforceResource::where('user_id', '=', $id)
                                         ->firstOrFail();
        $dt          = Carbon::now();
        $date        = $dt->toIso8601String();
        $objectId    = Input::get('projectRevisionId');

        $time = $this->addTime(Input::get('currentTime'), Input::get('productionHours'));
        $notes = $this->addDetails(Carbon::now()->toDateString(), Input::get('currentNotes'), Input::get('details'));
        $updateArray = [
          "Date_Completed__c" => $date,
          "Time_Hrs__c"       => $time,
          "Notes__c"          => $notes,
          "Status__c"         => 'PM Reviewing'
        ];

        $response = $this->updateObject('Project_Revisions__c', $objectId,
          $updateArray);

        $this->projectRevisions->getProjectRevisions();
        return $response;
    }

    public function addTime($oldTime, $newTime){
        $time = (int)$oldTime + (int)$newTime;
        return $time;
    }

    public function addDetails($date, $oldNotes, $newNotes){
        $notes = $date .': '. $newNotes . '<br>' . $oldNotes; 
        return $notes;
    }


}
