<?php

namespace Ecreativeworks\Salesforce\Api\Post;

use Auth;
use Redis;
use Request;
use DateTime;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\SalesforceResource;
use Illuminate\Support\Facades\Input;
use Ecreativeworks\Salesforce\Api\Get\Projects;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;

class SaveProjectTime extends SalesforceConnector
{

    /**
     * @var \Request
     */
    private $request;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\Projects
     */
    private $projects;

    public function __construct(Client $client, Request $request, Projects $projects)
    {
        parent::__construct($client);
        $this->request = $request;
        $this->projects = $projects;
    }

    public function saveProject()
    {

        if (!Auth::check()) {
            return 'Please Login';
        }
        $user = Auth::user();
        $id = $user->id;
        $resource = SalesforceResource::where('user_id', '=', $id)->firstOrFail();
        $dt = Carbon::now();
        $date = $dt->toIso8601String();
        $objectId = Input::get('projectid');
        $updateArray = [
            "Type_of_Production_Task__c" => Input::get('time_option'),
            "Production_Hours__c" => Input::get('productionHours'),
            "Details__c" => Input::get('details'),
            "SFDC_Resource__c" => $resource->resource_id,
            "Production_Task_Completed_Date__c" => $date,
            "SFDC_Projects__c" => $objectId
        ];

        $response = $this->saveObject('SFL5_Time__c', $updateArray);

        //update to in review if in review is checked
        if (Input::get('review') == true) {
            $statusResponse = $this->updateObject('SFL5_Projects__c', $objectId, ['Project_Stage__c' => 'Sales/Technical Review']);
        }
        $getProjectsResponse = $this->projects->getProjects();

        return $response;
    }


}