<?php

namespace Ecreativeworks\Salesforce\Api\Post;

use Auth;
use Request;
use Carbon\Carbon;
use GuzzleHttp\Client;
use App\SalesforceResource;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\SalesforceApi;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use Ecreativeworks\Salesforce\Repositories\ResourceRepository;

class SaveTask extends SalesforceConnector
{

    /**
     * @var \Request
     */
    private $request;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\Projects
     */
    private $projectRevisions;
    /**
     * @var \Ecreativeworks\Salesforce\Repositories\ResourceRepository
     */
    private $resource;
    /**
     * @var \App\Http\Controllers\SalesforceApi
     */
    private $salesforceApi;

    public function __construct(
      Client $client,
      Request $request,
      ResourceRepository $resource,
      SalesforceApi $salesforceApi

    ) {
        parent::__construct($client);
        $this->request  = $request;
        $this->resource = $resource;
        $this->salesforceApi = $salesforceApi;
    }

    public function saveTask()
    {

        if (!Auth::check()) {
            return 'Login Fool!';
        }

        $dt          = Carbon::now();
        $date        = $dt->toIso8601String();
        $objectId    = Input::get('id');
        $objectType  = $this->resolveObjectType(Input::get('objectType'));
        $timeField   = $this->resolveTimeField(Input::get('objectType'));

        $time = $this->addTime(Input::get('currentTime'), Input::get('taskHours'));

        $notes = $this->addDetails(Carbon::now()->toDateString(), Input::get('currentNotes'), Input::get('details'));
        $updateArray = [
          "Date_Completed__c" => $date,
          $timeField          => $time,
          "Notes__c"          => $notes,
        ];

        if(Input::get('review') == true){
            $updateArray['Status__C'] = 'In Review';
        }
        $response = $this->updateObject($objectType, $objectId, $updateArray);
	
        $this->salesforceApi->getTasks();
        return $response;
    }

    public function addTime($oldTime, $newTime){
        $time = (float)$oldTime + (float)$newTime;
        return $time;
    }

    public function addDetails($date, $oldNotes, $newNotes){
        $notes = $date .': '. $newNotes . '<br>' . $oldNotes; 
        return $notes;
    }

    protected function resolveObjectType($object){
        switch($object){
            case 'pr':
                return 'Project_Revisions__c';
            break;
            case 'dr':
                return 'Design_Revisions__c';
                break;
            case 'dq':
                return 'Daily_Q__c';
            break;
            case 'seoq':
                return 'SEO_Q__c';
            break;
        }
    }

    protected function resolveTimeField($object){
        switch($object){
            case 'pr':
                return 'Time_Hrs__c';
                break;
            case 'dr':
                return 'Time_Hrs__c';
                break;
            case 'dq':
                return 'Actual_Time__c';
                break;
            case 'seoq':
                return 'Time__C';
                break;
        }
    }

}
