<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use GuzzleHttp\Client;


class ProjectRevisions extends SalesforceConnector
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProjectRevisions()
    {
        $queryString = "
	select
	name,
	CreatedDate,
	CreatedBy.Name,
	id,
	Project__r.name,
	Project__r.Id,
	Domain__c,
	Domain__r.Test_Site_Url__c,
	Domain__r.Domain_Name__c,
	Desired_Completion_Date__c,
	Description__c,
	Priority__c,
	Additional_Files_Are_Here__c,
	Status__c,
	Time_Hrs__c,
	Notes__c,
	Make_updates_live_upon_completion__c,
	Developer__r.id,
	Developer__r.Name
	from Project_Revisions__c
	where Status__c != 'Completed'
	and Status__c != 'On Hold'";
        $response    = json_decode($this->sendRequest($queryString));
        $this->storeRecords('projectrevisions', $this->parseResponse($response));
    }

}