<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use Redis;
use GuzzleHttp\Client;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;

class Dailyqs extends SalesforceConnector{

  public function __construct() {
	parent::__construct();
  }

  public function getDailyqs(){
	$queryString = "
	select
	Owner.Name,
	CreatedDate,
	id,
	Name,
	Description__c,
	Desired_Completion_Date__c,
	Domain__r.Name,
	Domain__r.Domain_Name__c,
	Domain__r.Id,
	Domain__r.Test_Site_Url__c,
	Account__r.Name,
	Make_updates_live_upon_completion__c,
	Link_to_Page_with_Issue__c,
	Priority__c,
	Quoted_Time_Developer_Quoted__c,
	Status__c,
	Time_Quote_Only_Needed__c,
	Resource__r.id,
	Resource__r.Name,
	Actual_Time__c,
	Notes__c
	from
	Daily_Q__c
	where Status__c != 'Completed'";
	$response = json_decode($this->sendRequest($queryString));
	$this->storeRecords('dailyqs', $this->parseResponse($response));
  }

}