<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use Auth;
use GuzzleHttp\Client;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use Ecreativeworks\Salesforce\Repositories\ResourceRepository;

class Resources extends SalesforceConnector{

  /**
   * @var \Ecreativeworks\Salesforce\Repositories\ResourceRepository
   */
  private $repository;

  public function __construct(Client $client, ResourceRepository $repository) {
	parent::__construct($client);
	$this->repository = $repository;
  }

  public function getResources(){
	$user = Auth::user();
	$queryString = "select id, Name from SFL5_Resource__c where Name = '{$user->name}'";

	$response = json_decode($this->sendRequest($queryString));
	$resource = [];
	foreach($response->records as $record){
	  $resource = [
		'id'	=> $record->Id,
		'name'	=> $record->Name
	  ];
	}

	$this->repository->createResource($resource, $user);
	return $resource;

  }


}