<?php
/**
 * Created by PhpStorm.
 * User: ckipp01
 * Date: 10/13/17
 * Time: 7:08 PM
 */

namespace Ecreativeworks\Salesforce\Api\Get;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;

class Conflicting extends SalesforceConnector
{
    public function __construct()
    {
        parent::__construct();
    }

    public function checkConflicting($id = '') {
        if ($id == '') {
            $id = Input::get('id');
        }
        $queryString = "SELECT Id, Name, CreatedDate, Description__c, Resource__r.Name, Owner.Name,  Status__c
                        from SEO_Q__c
                        where Domain__c = '{$id}' 
                        and Status__c != 'Completed'";
        // This will get the conflicting SEO Q's
        $response = $this->sendRequest($queryString);

        $queryString = "SELECT Id, Name, CreatedDate, Project_Description__c, Developer__r.Name, Project_Owner__r.Name, Project_Stage__c, 
                        (SELECT id, name, Domain__c from Project_Domains__r )
                         from SFL5_Projects__c 
                         where Project_Stage__c = 'In Development' 
                         or Project_Stage__c = 'Sales/Technical Review' 
                         or Project_Stage__c = 'Client Revisions'";
        //This will get the conflicting projects
        $response2 = $this->sendRequest($queryString);

        $queryString = "SELECT Id, Name, CreatedDate, Description__c, Resource__r.Name, Owner.Name, Status__c
                        from Daily_Q__c 
                        where Status__c != 'Completed' 
                        and Domain__c = '{$id}'";
        //This will get the conflicting daily Q's
        $response3 = $this->sendRequest($queryString);
        $test = [];
        $test[] = $response;
        $test[] .= $response2;
        $test[] .=$response3;

        return $test;
    }
}
