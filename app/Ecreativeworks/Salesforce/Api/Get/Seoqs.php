<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use Redis;
use GuzzleHttp\Client;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;

class Seoqs extends SalesforceConnector{

  public function __construct() {
	parent::__construct();
  }

  public function getSeoqs(){
	$queryString = "
	select
	Account__r.Name,
	Id,
	CreatedDate,
	owner.name,
	name,
	Account__c,
	Time__C,
	Notes__c,
	Date_Completed__c,
	Desired_Completion_Date__c,
	Resource__r.id,
	Resource__r.Name,
	Description__c,
	Domain__r.Name,
	Domain__r.Domain_Name__c,
	Domain__r.Id,
	Domain__r.Test_Site_Url__c,
	Make_updates_live_upon_completion__c,
	Priority__c,
	Project__c,
	Estimated_Time__c,
	Status__c,
	Time_Quote_Only_Needed__c
	from SEO_Q__c where Status__c !='Completed'";

	$resp = json_decode($this->sendRequest($queryString));
	$response = json_decode($this->sendRequest($queryString));
	$this->storeRecords('seoqs', $this->parseResponse($response));
  }

}