<?php
/**
 * Created by PhpStorm.
 * User: ckipp01
 * Date: 10/13/17
 * Time: 7:08 PM
 */

namespace Ecreativeworks\Salesforce\Api\Get;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;

class Domains extends SalesforceConnector
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDomains() {
        $queryString = "SELECT Id, Name, Domain_Name__c FROM Domain__c WHERE LastModifiedDate = LAST_N_DAYS:365";

        $response = $this->sendRequest($queryString);

        return $response;
    }
}