<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use App\Options;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Redis;

class TimeOptions extends SalesforceConnector {

  protected $timeGroups = [
	'Type_of_Production_Task__c',
	'Type_of_SEO_Task__c',
	'Type_of_Task__c',
	'Project_Manager__c'
  ];

  protected $timeOptions;

  public function __construct() {
	parent::__construct();
	$this->init();
  }

	public function init(){
	  $this->getTime();
	  $this->saveToMySql();
	}

  public function getTime() {
	$response          = $this->describeObject('SFL5_Time__c');
	$response          = json_decode($response);
	$this->timeOptions = $this->iterateTime($response->fields);
	return $this->timeOptions;

  }

  protected function iterateTime($timeArray) {
	$returnArray = [];
	foreach ($timeArray as $record) {
	  if (is_array($record)) {
		$this->iterateTime($record);
	  }
	  elseif (is_object($record)) {
		if (isset($record->name) && in_array($record->name, $this->timeGroups)) {
		  $returnArray[$record->name] = $this->extractTimeOptions($record);
		}
	  }
	}
	return $returnArray;
  }

  protected function extractTimeOptions($recordObject) {
	$picklistValues = $recordObject->picklistValues;
	$values         = [];
	foreach ($picklistValues as $value) {
	  $values[$value->label] = $value->value;
	}
	return $values;
  }


  protected function saveToMySql() {
	$options = json_encode($this->timeOptions);
	Options::updateOrCreate([
		'key'   => 'timeOptions',
		'value' => $options
	  ]
	);
  }

}