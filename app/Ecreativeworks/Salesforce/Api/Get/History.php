<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\SalesforceApi;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;


class History extends SalesforceConnector{

    public function __construct() {
        parent::__construct();
    }

    public function getHistory(){
        $history = [];

        $domain     = Input::get('queryDomain');
        $resource   = Input::get('resource');
        $type       = Input::get('type');

//        if (in_array('SFL5_Projects__c', $type)) {
//            $pQueryString   = "SELECT Id, Name, (SELECT Domain__r.Name from Project_Domains__r) from SFL5_Projects__c";
//            if (isset($resource) && $resource !== "All Resources") {
//                $pQueryString .= " WHERE Developer__r.Name = '{$resource}'";
//            }
//            $pResponse = $this->sendRequest($pQueryString);
//            $history[] = $pResponse;
//        }
        if (in_array('Project_Revisions__c',$type)) {
            $PRString = "SELECT Id, Name, Developer__r.Name, Domain__r.Name, Notes__c, Date_Completed__c from Project_Revisions__c";
            if (isset($resource) && $resource !== "All Resources") {
                $PRString .= " WHERE Developer__r.Name = '{$resource}'";
            }
            if (isset($domain) && $domain !== "All Domains" && $resource === "All Resources") {
                $PRString .= " WHERE Domain__r.Id = '{$domain}'";
            } elseif (isset($domain) && $domain !== "All Domains") {
                $PRString .= " AND Domain__r.Id = '{$domain}'";
            }
            $PRString .= " ORDER BY Date_Completed__c DESC NULLS LAST LIMIT 100";
            $PRResponse = $this->sendRequest($PRString);
            if ($history) {
                $history[] .= $PRResponse;
            } else {
                $history[] = $PRResponse;
            }
        }
        if (in_array('Daily_Q__c',$type)) {
            $dqRawString = "SELECT Id, Name, Resource__r.Name, Domain__r.Name, Notes__c, Date_Completed__c from Daily_Q__c";
            if (isset($resource) && $resource !== "All Resources") {
                $dqRawString .= " WHERE Resource__r.Name = '{$resource}'";
            }
            if (isset($domain) && $domain !== "All Domains" && $resource === "All Resources") {
                $dqRawString .= " WHERE Domain__r.Id = '{$domain}'";
            } elseif (isset($domain) && $domain !== "All Domains") {
                $dqRawString .= " AND Domain__r.Id = '{$domain}'";
            }
            $dqRawString .= " ORDER BY Date_Completed__c DESC NULLS LAST LIMIT 100";
            $dqResponse = $this->sendRequest($dqRawString);
            if ($history) {
                $history[] .= $dqResponse;
            } else {
                $history[] = $dqResponse;
            }
        }
        if (in_array('SEO_Q__c',$type)) {
            $rawSEOString = "SELECT Id, Name, Resource__r.Name, Domain__r.Name, Notes__c, Date_Completed__c from SEO_Q__c";
            if (isset($resource) && $resource !== "All Resources") {
                $rawSEOString .= " WHERE Resource__r.Name = '{$resource}'";
            }
            if (isset($domain) && $domain !== "All Domains" && $resource === "All Resources") {
                $rawSEOString .= " WHERE Domain__r.Id = '{$domain}'";
            } elseif (isset($domain) && $domain !== "All Domains") {
                $rawSEOString .= " AND Domain__r.Id = '{$domain}'";
            }
            $rawSEOString .= " ORDER BY Date_Completed__c DESC NULLS LAST LIMIT 100";
            $seoResponse = $this->sendRequest($rawSEOString);
            if ($history) {
                $history[] .= $seoResponse;
            } else {
                $history[] = $seoResponse;
            }
        }
        return $history;
    }
}