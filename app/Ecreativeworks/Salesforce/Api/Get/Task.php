<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use Ecreativeworks\Salesforce\Api\Get\Seoqs;
use Ecreativeworks\Salesforce\Api\Get\Dailyqs;
use Ecreativeworks\Salesforce\Api\Get\ProjectRevisions;


class Task extends SalesforceConnector{

    public function __construct() {
        parent::__construct();
    }

    public function getFtpInfo($id = ''){
        if($id == '') {
            $id = Input::get('id');
        }
        $queryString = "Select FTP_Host__c, FTP_Username__c, FTP_Password__c, FTP_URL__c, Domain_Name__c, Domain_Notes__c, Database_Username__c,
                        Database_Password__c, CMS_Username__c, CMS_Password__c, Cart_Admin_Username__c, Cart_Admin_Password__c 
                        from Domain__c 
                        where id ='{$id}'";
        $response = $this->sendRequest($queryString);
        if(isset($response['code']) && $response['code'] == '400'){
            return [
              'error' => 'No Domain is setup on task, or you do not have permissions to view the domains in Salesforce'
            ];
        }
        $response = json_decode($response);
        $connectionInfo = [];
        foreach ($response->records as $record){
            $connectionInfo['ftp_host'] 			= $record->FTP_Host__c;
            $connectionInfo['ftp_user'] 			= $record->FTP_Username__c;
            $connectionInfo['ftp_password'] 		= $record->FTP_Password__c;
	        $connectionInfo['ftp_url']			    = $record->FTP_URL__c;
            $connectionInfo['database_user'] 		= $record->Database_Username__c;
            $connectionInfo['database_password'] 	= $record->Database_Password__c;
            $connectionInfo['cart_admin'] 		    = $record->Cart_Admin_Username__c;
            $connectionInfo['cart_password'] 		= $record->Cart_Admin_Password__c;
            $connectionInfo['cms_admin'] 			= $record->CMS_Username__c;
            $connectionInfo['cms_password'] 		= $record->CMS_Password__c;
            $connectionInfo['domain_notes'] 		= $record->Domain_Notes__c;
        }
        return $connectionInfo;
    }
}