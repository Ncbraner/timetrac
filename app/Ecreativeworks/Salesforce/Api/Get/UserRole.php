<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use App\User;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;



class UserRole extends SalesforceConnector
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $user  is instance of User Model
     *
     * @return mixed
     */
    public function getRoleForUser($user)
    {
        $queryString = "Select ECW_Time_App_Role__c from User where Id = '".$user->salesforce_id."' ";
        $response    = json_decode($this->sendRequest($queryString));
        User::where('salesforce_id', $user->salesforce_id)
            ->update(['role' => $response->records[0]->ECW_Time_App_Role__c]);

        return $response;
    }



}