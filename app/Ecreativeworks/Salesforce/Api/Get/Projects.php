<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Input;
use Ecreativeworks\Salesforce\Api\SalesforceConnector;

class Projects extends SalesforceConnector{

  public function __construct() {
	parent::__construct();
  }

  public function getProjects(){
	$queryString = "
	SELECT
	name,
	CreatedDate,
	id,
	RecordType.Name,
	Project_Owner__r.Name,
	Project_Stage__c,
	Project_Description__c,
	Developer__r.id,
	Developer__r.Name,
	Test_Site_URL__c,
  Egnyte_Url__c,
	Buildout_Due_Date__c,
	Date_Moved_into_Production_Date__c,
	Quote_Design_Hours__c,
	Quoted_Developer_Hours__c,
	Quoted_Production_Hours_Rollup__c,
	Design_Time__c,
	Total_Developer_Hours__c,
	Total_Production_Hours__c,
	Used_Project_Hours__c
	FROM SFL5_Projects__c
	WHERE Project_Stage__c != 'Completion'
	AND Project_Stage__c != 'Project Cancelled'
	AND Project_Stage__c != 'SEO Year Completed'";
	$response = $this->sendRequest($queryString);
	
	if(is_string($response)){
	    $response_array = json_decode($response);
	    $this->storeRecords('projects', json_encode($this->parseResponse($response_array)));
    }
    return $response;
  }

    public function prepareLink($url)
    {
        if (strpos($url,'http') !== false) {
            return $url;
        } elseif (strpos($url,'www') === 0) {
            return 'http://' . $url;
        } elseif (strpos($url, 'www') === false && $url !== null) {
            return 'http://www.' . $url;
        } else {
            return 'Not set or not a valid url ¯\_(ツ)_/¯';
        }
    }

  public function getFtpInfo($projectId = ''){
	  if($projectId == '') {
		  $projectId = Input::get('id');
	  }
	$queryString = "Select id, (Select id, name from Project_Domains__r ) from SFL5_Projects__c where id = '{$projectId}'";
	$response = json_decode($this->sendRequest($queryString));
	  if($response->records[0]->Project_Domains__r != '') {
		  $domain_id = $response->records[0]->Project_Domains__r->records[0]->Id;
	  }else{
		  return [
			  'error' => 'No Domain is setup on project'
		  ];
	  }
	$domainInfoQueryString = "select Domain__c from Project_Domain__c where id= '{$domain_id}'";
	$domainInfo = json_decode($this->sendRequest($domainInfoQueryString));
	$domainId = $domainInfo->records[0]->Domain__c;
	$queryString = "Select FTP_Host__c, FTP_URL__c, FTP_Username__c, FTP_Password__c, Domain_Name__c, Domain_Notes__c, Database_Username__c, Database_Password__c, CMS_Username__c, CMS_Password__c, Cart_Admin_Username__c, Cart_Admin_Password__c, Test_Site_Url__c from Domain__c where id ='{$domainId}'";
	$response = json_decode($this->sendRequest($queryString));
	$connectionInfo = [];

	foreach ($response->records as $record){
	  $connectionInfo[$projectId]['ftp_host'] 					= $record->FTP_Host__c;
	  $connectionInfo[$projectId]['ftp_url']            = $record->FTP_URL__c;
	  $connectionInfo[$projectId]['ftp_user'] 					= $record->FTP_Username__c;
	  $connectionInfo[$projectId]['ftp_password'] 			= $record->FTP_Password__c;
	  $connectionInfo[$projectId]['database_user'] 			= $record->Database_Username__c;
	  $connectionInfo[$projectId]['database_password'] 	= $record->Database_Password__c;
	  $connectionInfo[$projectId]['cart_admin'] 				= $record->Cart_Admin_Username__c;
	  $connectionInfo[$projectId]['cart_password'] 			= $record->Cart_Admin_Password__c;
	  $connectionInfo[$projectId]['cms_admin'] 					= $record->CMS_Username__c;
	  $connectionInfo[$projectId]['cms_password'] 			= $record->CMS_Password__c;
	  $connectionInfo[$projectId]['domain_notes'] 			= $record->Domain_Notes__c;
	  $connectionInfo[$projectId]['live_url']           = $this->prepareLink($record->Domain_Name__c);
	  $connectionInfo[$projectId]['test_url']	 					= $this->prepareLink($record->Test_Site_Url__c);
	}
	return $connectionInfo;
  }
}
