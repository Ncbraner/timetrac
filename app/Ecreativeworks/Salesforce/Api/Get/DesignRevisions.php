<?php

namespace Ecreativeworks\Salesforce\Api\Get;

use Ecreativeworks\Salesforce\Api\SalesforceConnector;
use GuzzleHttp\Client;


class DesignRevisions extends SalesforceConnector
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDesignRevisions()
    {
        $queryString = "
	select
	name,
	CreatedDate,
	CreatedBy.Name,
	id,
	Project__r.name,
	Project__r.Id,
	Domain__c,
	Domain__r.Test_Site_Url__c,
	Desired_Completion_Date__c,
	Description__c,
	Priority__c,
	Additional_Files_Are_Here__c,
	Status__c,
	Time_Hrs__c,
	Notes__c,
	Make_updates_live_upon_completion__c,
	Developer__r.id,
	Developer__r.Name
	from Design_Revisions__c
	where Status__c != 'Completed'
	and Status__c != 'On Hold'";
        $queryResult = $this->sendRequest($queryString);
        if (!is_array($queryResult)) {
            $response = json_decode($queryResult);
            $this->storeRecords('designrevisions', $this->parseResponse($response));
        }
    }
}
