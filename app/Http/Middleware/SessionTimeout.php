<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Session\Store;
use Illuminate\Support\Facades\Auth;

class SessionTimeout
{
    protected $session;
    protected $timeout = 600; //should be using TIMEOUT from .env file

    public function __construct(Store $session)
    {
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('users')) {
            return redirect('/')->with('status', 'You have been logged out');
        }

        if (!$this->session->has('lastActivityTime')) {
            Auth::logout();
            $this->session->put('lastActivityTime', time());
        } elseif (time() - $this->session->get('lastActivityTime') > $this->getTimeOut()) {
            $this->session->forget('lastActivityTime');
            Auth::logout();
            return redirect('/')->with('status', 'You have been logged out');
        }
        $this->session->put('lastActivityTime', time());
        return $next($request);
    }

    protected function getTimeOut()
    {
        return (env('TIMEOUT')) ?: $this->timeout;
    }
}
