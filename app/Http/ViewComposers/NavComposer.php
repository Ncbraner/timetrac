<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class NavComposer
{
    protected $menu;

    public function __construct(NavMenuRepository $menu)
    {
        $this->menu = $menu;
    }

    public function compose(View $view)
    {
        $thing= $this->menu->thing();
        $somethingElse = $this->menu->somethingElseForMyDatabase();

        $view->with(compact('thing', 'somethingElse'));
    }
}
