<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        $user = Auth::user();
        return view('pages.home', array('user' => $user));
    });
    Route::get('login', ['as'=>'login', 'uses'=>'Auth\AuthController@login']);
    Route::get('login/callback', 'Auth\AuthController@login');
    Route::get('logout', ['as'=>'logout', 'uses'=>'Auth\AuthController@logout']);

});


Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('projects', 'PagesController@projects');
    Route::get('tasks', 'PagesController@tasks');

    Route::get('history', 'PagesController@history');

    //Reports
    Route::get('reports/projects', 'PagesController@projectsReport');

    Route::get('updateall', 'JobsController@updateTasks');

});

Route::group(['middleware' => ['web', 'auth']], function () {

    //api end points
    Route::get('api/get/seoqs/', 'SalesforceApi@getSeoqs');
    Route::get('api/get/dailyqs/', 'SalesforceApi@getDailyqs');
    Route::get('api/get/projects/', 'SalesforceApi@getProjects');
    Route::get('api/get/resources/', 'SalesforceApi@getResources');
    Route::get('api/get/projectrevisions/', 'SalesforceApi@getProjectRevisions');
    Route::get('api/get/timeoptions', 'SalesforceApi@getTime');
    Route::get('api/get/domains', 'SalesforceApi@getDomains');

    //get all prs, seoqs, dailyqs
    Route::get('api/get/tasks', 'SalesforceApi@getTasks');

    Route::get('/api/post/projectftp', 'SalesforceApi@projectFtp');

    //update api routes
    Route::post('api/post/updateproject', 'SalesforceApi@updateProject');
    Route::post('api/post/updatetask', 'SalesforceApi@updateTask');

    //ftp info routes
    Route::post('api/post/projectftp', 'SalesforceApi@projectFtp');
    Route::post('/api/post/taskftpinfo', 'SalesforceApi@taskFtp');

    //Check conflicting tasks routes
    Route::post('api/post/checkconflicting', 'SalesforceApi@checkConflicting');

    //Get History
    Route::post('api/post/history', 'SalesforceApi@history');
});

Route::group(['middleware' => ['web', 'auth']], function () {
    Route::post('session/ajaxCheck', ['uses' => 'SessionController@ajaxCheck', 'as' => 'session.ajax.check']);
    Route::post('session/ajaxExtend', ['as' => 'session.ajax.extend', function(){
        Session::set('lastActive', date('U'));
        Session::forget('idleWarningDisplayed');
        Session::forget('logoutWarningDisplayed');
    }]);

});