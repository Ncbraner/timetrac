<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Javascript;
use Carbon\Carbon;
use App\Http\Requests;
use Ecreativeworks\Salesforce\Library\PrepareDataForView;
use Ecreativeworks\Salesforce\Repositories\ResourceRepository;


class PagesController extends Controller
{
    /**
     * @var \Ecreativeworks\Salesforce\Library\PrepareDataForView
     */
    private $viewData;


    public function __construct(PrepareDataForView $viewData)
    {
        $this->viewData = $viewData;
    }

    public function projects()
    {
        $data = $this->viewData->execute('projects');
	
        JavaScript::put([
          'data'          => $data['data'],
          'salesforce_id' => $data['salesforce_id'],
          'timeOptions'   => $data['timeOptions'],
          'role'          => Auth::user()->role      
        ]);
        return view('pages.projects');
    }

    public function tasks(Carbon $carbon, ResourceRepository $resource){

        $data = $this->viewData->tasks($carbon);

        Javascript::put([
            'data'      => $data,
            'resource'  => $resource->getResource(),
            'task'      => new \stdClass(),
            'role'      => Auth::user()->role
        ]);
        return view('pages.tasks');
    }

    public function projectRevisions()
    {
        $data = $this->viewData->execute('projectrevisions');
        JavaScript::put([
          'data'          => $data['data'],
          'salesforce_id' => $data['salesforce_id'],
          'role'          => Auth::user()->role
        ]);
        return view('pages.projectRevisions');
    }

    public function projectsReport(Carbon $carbon){
        $data = $this->viewData->returnProjectReportData('projects', $carbon);
        JavaScript::put([
          'data'          => $data,
          'role'          => Auth::user()->role
        ]);

        return view('pages.reports.projects');
    }

    public function history()
    {
        $data = $this->viewData->execute('history');

        JavaScript::put([
            'data' => $data,
            'role'          => Auth::user()->role,
        ]);
        return view('pages.history');
    }
}
