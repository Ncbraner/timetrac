<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Ecreativeworks\Salesforce\SalesforceAuthenticationHandler;

class AuthController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Registration & Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles the registration of new users, as well as the
  | authentication of existing users. By default, this controller uses
  | a simple trait to add these behaviors. Why don't you explore it?
  |
  */

  use AuthenticatesAndRegistersUsers, ThrottlesLogins;

  /**
   * Where to redirect users after login / registration.
   *
   * @var string
   */
  protected $redirectTo = '/';
  /**
   * @var \Socialite
   */
  private $socialite;
  /**
   * @var \Ecreativeworks\Salesforce\SalesforceAuthenticationHandler
   */
  private $salesforceAuthenticationHandler;

  /**
   * Create a new authentication controller instance.
   *
   * @return void
   */
  public function __construct(SalesforceAuthenticationHandler $salesforceAuthenticationHandler)
  {
	$this->middleware('guest', ['except' => 'logout']);
	$this->salesforceAuthenticationHandler = $salesforceAuthenticationHandler;
  }


  public function login(){

	return $this->salesforceAuthenticationHandler->doLogin($this->redirectTo);
  }

  /**
   * Obtain the user information from Salesforce.
   *
   * @return Response
   */

  public function logout(){
	$this->salesforceAuthenticationHandler->logout();
	return Redirect::to($this->redirectTo)->with('status', 'You have been logged out');
  }

}
