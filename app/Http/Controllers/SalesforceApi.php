<?php

namespace App\Http\Controllers;

use Ecreativeworks\Salesforce\Api\Get\Domains;
use Ecreativeworks\Salesforce\Api\Get\Conflicting;
use Ecreativeworks\Salesforce\Api\Get\History;
use Illuminate\Http\Request;
use Ecreativeworks\Salesforce\Api\Get\Task;
use Ecreativeworks\Salesforce\Api\Get\Seoqs;
use Ecreativeworks\Salesforce\Api\Get\Dailyqs;
use Ecreativeworks\Salesforce\Api\Get\Projects;
use Ecreativeworks\Salesforce\Api\Get\Resources;
use Ecreativeworks\Salesforce\Api\Get\TimeOptions;
use Ecreativeworks\Salesforce\Api\Post\SaveTask;
use Ecreativeworks\Salesforce\Api\Get\DesignRevisions;
use Ecreativeworks\Salesforce\Api\Get\ProjectRevisions;
use Ecreativeworks\Salesforce\Api\Post\SaveProjectTime;
use Ecreativeworks\Salesforce\Repositories\ResourceRepository;
use Ecreativeworks\Salesforce\Api\Post\SaveProjectRevisionTime;

class SalesforceApi extends Controller
{
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\ProjectRevisions
     */
    private $pr;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\Dailyqs
     */
    private $dq;
    /**
     * @var \Ecreativeworks\Salesforce\Api\Get\Seoqs
     */
    private $seoq;

    /**
     * @var \Ecreativeworks\Salesforce\Api\SalesforceConnector
     */

    public function __construct()
    {

    }

    /**
     * Create resources and match them to their app user
     */
    public function getResources(Resources $api, ResourceRepository $repo)
    {
        $resources = $api->getResources();

    }

    public function getProjects()
    {

        $api = new Projects();
        $projects = $api->getProjects();

    }

    public function getProjectRevisions()
    {
        $pr = new ProjectRevisions();
        $projectRevisions = $pr->getProjectRevisions();

    }

    public function getDailyqs()
    {
        $dq = new Dailyqs();
        $dailyqs = $dq->getDailyqs();

    }

    public function getSeoqs()
    {
        $seoq = new Seoqs();
        $seoq = $seoq->getSeoqs();
    }

    public function getDesignRevisions()
    {
        $designRevisions = new DesignRevisions();
        $designRevisions = $designRevisions->getDesignRevisions();
    }
    public function getTasks()
    {
        $this->getProjectRevisions();
        $this->getDailyqs();
        $this->getSeoqs();
        $this->getDesignRevisions();
    }

    public function getTime(TimeOptions $api)
    {
        $options = $api->getTime();
    }

    public function updateProject(SaveProjectTime $api)
    {
        return $updatedProject = $api->saveProject();
    }

    public function taskFtp(Task $api)
    {
        return $api->getFtpInfo();
    }

    public function projectFtp(Projects $api)
    {
        return $api->getFtpInfo();
    }

    public function updateProjectRevision(SaveProjectRevisionTime $api)
    {
        return $api->saveProjectRevision();
    }

    public function updateTask(SaveTask $api){
        return $api->saveTask();
    }

    public function checkConflicting(Conflicting $api) {
        return $api->checkConflicting();
    }

    public function getDomains(Domains $api) {
        return $api->getDomains();
    }

    public function history(History $api) {
        return $api->getHistory();
    }
}
