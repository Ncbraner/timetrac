<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
use Auth;

class SessionController extends Controller
{

    public function ajaxCheck()
    {
    	if(!Auth::check()){
            Session::flush();
            Session::set('logoutWarningDisplayed', true);
            return 'loggedOut';
        }
        $sessionTimeoutInSeconds = config('session.lifetime') * 60;
        $idleTimeBeforeWarning = (config('session.lifetime') * .8) * 60;  //80% of session lifetime
        $maxIdleBeforeLogout = $sessionTimeoutInSeconds * 1;

        $maxIdleBeforeWarning = $idleTimeBeforeWarning * 1;
        $warningTime = $maxIdleBeforeLogout - $maxIdleBeforeWarning;
        // Calculate the number of seconds since the use's last activity
        $idleTime = date('U') - Session::get('lastActive');
        // Warn user they will be logged out if idle for too long
        if ($idleTime > $maxIdleBeforeWarning && empty(Session::get('idleWarningDisplayed'))) {

            Session::set('idleWarningDisplayed', true);

            return 'You have ' . $warningTime / 60 . ' minutes left before you are logged out';
        }

        // Log out user if idle for too long
        if ($idleTime > $maxIdleBeforeLogout && empty(Session::get('logoutWarningDisplayed'))) {

            // *** Do stuff to log out user here
            Session::flush();
            Session::set('logoutWarningDisplayed', true);

            return 'loggedOut';
        }

        return '';
    }
}