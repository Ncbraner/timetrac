<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jobs\UpdateTasks;


class JobsController extends Controller
{
    public function updateTasks(UpdateTasks $job){
        $this->dispatch($job);
    }
}
