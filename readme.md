# Timetrack

>Timetrack is built with Laravel 5.2 in the backend and Vue 2.4 in the front.

[Laravel website](http://laravel.com/docs) --
[Vue website](https://vuejs.org/
)
#### Understanding what is going on in the front end
>The front end is using [Gulp](https://gulpjs.com/) as a task runner. You'll notice in the gulp.js file in the root
that we are utilizing Laravel's built in [Elixir](https://laravel.com/docs/5.2/elixir) and the [laravel-elixir-vueify](https://github.com/JeffreyWay/laravel-elixir-vueify) library with it.
Basically it's a wrapper for Elixr, Browserify, and Vueify. This means that we can write singe file Vue components and have
browserify compile them into Javascript that the browser can understand. It also is taking care of the SASS. You'll notice in the file
that it will be pulling from the resources/assets and either /js or /css for files and spitting the out in the /public directory.
##### Userful Commands
` $ gulp ` --> this will just compile everything down

` $ gulp watch ` --> This wil basically give you hot reloading as you develop so you don't have to keep doing `gulp` all of the time

` $ gulp --production ` --> this will bundle everything from production and minify all of you files.
> Keep in mind that before you bundle for production you'll want to set the APP_ENV in your .env file to production to make sure the Vue developer tool aren't active in production

#### Backend stuff
> First things first, the OAuth that  you see in the application is powered by an awesome little library call [Socialite](https://github.com/laravel/socialite). Check out the docs and as they'll come in handy if you 
need to change anything with that.

> You'll notice that in the front end you have access to the an object called Ecreativeworks. You can actually put Ecreativeworks in the console as well, and get the object spit out at you.
This is being done by [this php-vars-to-javascript library](https://github.com/laracasts/PHP-Vars-To-Js-Transformer). Take a look at the config/javascript.php file to see where it's being bound and what it's being called.

#####Cron Jobs
>Cron jobs are done a bit different in laravel.  In app/Console/Kernel.php you'll see where the cron job is defined. This is where you define how often the cron job runs,
what job is going to be running, and where you send the output to. The only think you should need to put in the crontab is below:

`* * * * * php /path-to-your-project/artisan schedule:run >> /dev/null 2>&1`

>You can find more infomation about laravel's [task scheduling here.](https://laravel.com/docs/5.2/scheduling)

## Development Environments
> For laravel applications there are a couple different environments that are quite easy to set up and utilize. There are some extra benifits
in doing so. For example, in timetrack you need a secure environment, or you won't be able to submit anything back to 
salesforce. With Valet, you can simply `valet secure laravel` and you have a secure environment. It's pretty awesome. Below are a few notes
on explaining the environments if you choose to go that route.

### Using [Valet](https://laravel.com/docs/5.4/valet)

> I'm using Valet to develop locally on timetrack. It's an incredible little
development tools that just runs Nginx in the background on your mac. Keep in mind
that you can't have Apache running on port 80 if you're using this. The instructions for isntalling and the docs that Laravel
has for this are golden, read them. They'll help you more than I will.

>Valet is only supposed to be for mac, but there is also a [Linux Version](https://github.com/cpriego/valet-linux) that works quite well that someone forked and created.

>There is also a [Windows Version](https://github.com/cretueusebiu/valet-windows), but for me, it was the only one that I couldn't get working correctly.

### *IF* you're going to use [Homestead](https://laravel.com/docs/5.4/homestead)

>Quck note, using Hometead probably seems like the best solution for development on different
operating systems... with that being said, you'll run into a problem with your local environment
not being https. here are ways around it, but setting up Valet was quite a bit easier. However, if you
are going to go the Homestead (Vagrant) route, then I have instructions on setting it up down below.

#### Getting Vagrant Set Up
___
1. You'll want to head over to [Vagrant](https://www.vagrantup.com/intro/getting-started/) and just do a quick read through their docs, they are really thorough.
You won't have to make a box or anything like that, you'll just need to get it installed. I'll have the Vagrantfile already made, so you'll literally just have to
`vagrant up` in the project directory and it will make the box for you with all of the settings you'll need.
2. You'll notice there is a vagrantfile and a homstead.yaml file in the root. That is what will explain what your box and settings will look like. You'll want to take the IP
address at the top of the yaml file and put it in your hosts as something like homestead.app which is what I have mine as. Then you'll need to set up your database. MySQL is located at port 33060.
You'll see an example .env file that I have in there that you'll need to make your .env file and change the necessary db settings.
The default username and password is what is currently in there.
3. Also, jump into config/app.php and make sure the env is set to development.
4. If you haven't yet, make sure to `composer install` in the project directory
5. Once the db is set up, your information is in the .env file, you should be able to `vagrant up` and then hit the homepage at homestead.app or whateever you put in your hosts file.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the .

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
