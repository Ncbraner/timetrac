//This is what is setting active class on the menu. Really didn't want include another file for it, so until
//jquery gets ripped out of here, this will just live here for the time being
$(document).ready(function () {
    $(".nav li").removeClass("active");

    var pathname = (window.location.pathname);
    var id = pathname.replace(/\//g, '');

    $('#' + id).addClass('active');
});
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});
//Actual purpose of this file/name below
jQuery(function(){
    var url = window.location.href;
    if( url.indexOf('.dev') != -1 ){
        var env = {
            "siteUrl": "https://timetrack-live.dev"
        }
    }else{
        var env = {
            "siteUrl": "https://timetrack.ecreativeworks.com"
        }
    }

    jQuery('body').on('click', '.refresh-projects-tasks', function(event){
        $('.refresh-projects-tasks > i').addClass('fa-spin');
        event.preventDefault();
        var tasks = jQuery.get(env.siteUrl + '/api/get/tasks');
        tasks.then(function(){
            console.log('retrieved tasks');
            var projects = jQuery.get(env.siteUrl + '/api/get/projects/');
            projects.then(function(){
                location.reload();
                console.log('retrieved projects');
            });
            projects.error(function(error){
                errorClassChange();
                alert("Refresh failed, please try to refresh again.");
            });
        });
        tasks.error(function(error){
            errorClassChange();
            console.log(error);
            alert("Refresh failed, please try to refresh again.");
        });
    });

    function successClassChange() {
        $('.refresh-projects-tasks > i').removeClass('fa-spin');
        $('.refresh-projects-tasks > i').addClass('has-success');

        setTimeout(function(){
            $('.refresh-projects-tasks > i').removeClass('has-success');
        }, 5000);
    }

    function errorClassChange() {
        $('.refresh-projects-tasks > i').removeClass('fa-spin');
        $('.refresh-projects-tasks > i').removeClass('fa-refresh');
        $('.refresh-projects-tasks > i').addClass('has-error');
        setTimeout(function(){
            $('.refresh-projects-tasks > i').removeClass('has-error');
        }, 5000);
    }

});
