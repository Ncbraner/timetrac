 // Vue.config.devtools = true;
    Vue.component('ecw-project-revisions-layout', {
        props: {
            'project': '',
        },
        template: '#project-revisions-template',

        data: function () {
            return {
                'ftp_host': '',
                'ftp_username':'',
                'ftp_password': '',
                'database_username':'',
                'database_password':'',
                'cart_admin_username':'',
                'cart_admin_password':'',
                'cms_admin_username':'',
                'cms_admin_password':'',
                'domain_notes':'',
                'showFtp': false,
                'showGetFtpButton': true,
                'showHideFtpButton': false,
                'ftpSpinner': false,
                'submitSpinner': false,
                'showModal': false,
                'productionHours':'',
                'details':''
            }
        },
        methods: {
            'getFtpInfo': function () {
                console.log(this.project.Project__r.Id);
                this.ftpSpinner         = true; //show ftp spinner at start of the function
                var postData = {
                    'projectid': this.project.Project__r.Id
                }
                var token = document.querySelector('#_token').getAttribute('value');
                var headers = {
                    'X-CSRF-TOKEN':  token
                }
                this.$http.post('/api/post/projectftp', postData, headers)
                        .then(function (response) {
                            var responseData =response.data[this.project.Project__r.Id];
                            //set all ftp/database/login fields

                            this.ftp_host               = responseData.ftp_host;
                            this.ftp_username           = responseData.ftp_user;
                            this.ftp_password           = responseData.ftp_password;
                            this.database_username      = responseData.database_user;
                            this.database_password      = responseData.database_password;
                            this.cart_admin_username    = responseData.cart_admin;
                            this.cart_admin_password    = responseData.cart_password;
                            this.cms_admin_username     = responseData.cms_user;
                            this.cms_admin_password     = responseData.cms_password;
                            this.domain_notes           = responseData.domain_notes;
                            //show ftp dropdown
                            this.showFtp                = true;
                            this.ftpSpinner             = false; //hide ftp spinner at end of function
                            //hide the getFtpButton
                            this.showGetFtpButton       = false;
                            this.showHideFtpButton      = true;
                        }, function (response) {
                            console.log(response.data);
                        });
            },
            'hideFtpInfo': function(){
                //hide ftp info
                this.showFtp                    = false;
                //show the getFtpButton
                this.showGetFtpButton           = true;
                this.showHideFtpButton          = false;
                //unset all the ftp info
                this.ftp_host                   = '';
                this.ftp_username               = '';
                this.ftp_password               = '';
                this.database_username          = '';
                this.database_password          = '';
                this.cart_admin_username        = '';
                this.cart_admin_password        = '';
                this.cms_admin_username         = '';
                this.cms_admin_password         = '';
                this.domain_notes               = '';
            },
            'addModal': function(){
                this.showModal = true;
                this.$broadcast('showModal', this.showModal);
            },
            closeModal: function(e){
                e.preventDefault();
                this.showModal = false;
                this.$broadcast('showModal', this.showModal);
            },
            saveProject: function (e) {
                e.preventDefault();
                this.submitSpinner = true;
                var postData = {
                    'productionHours': this.productionHours,
                    'details': this.details,
                    'projectRevisionId': this.project.Id,
                    'currentTime': this.project.Time_Hrs__c,
                    'currentNotes': this.project.Notes__c
                }
                var token = document.querySelector('#_token').getAttribute('value');
                var headers = {
                    'X-CSRF-TOKEN':  token
                }
                this.$http.post('/api/post/updateprojectrevision', postData, headers).then(function (response) {
                    this.submitSpinner = false;
                    //window.location.reload(false);
                    console.log(response);
                }, function (response) {
                    console.log(response.data);
                });
            },
        }
    });