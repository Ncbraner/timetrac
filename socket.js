var fs = require('fs');
var options = {
    key: fs.readFileSync('./key.pem', 'utf8'),
    cert: fs.readFileSync('./server.crt', 'utf8')
};
var server = require('https').Server(options);
var io = require('socket.io')(server);
var Redis = require('ioredis');
var redis = new Redis;

redis.subscribe('project-update');

redis.on('message', function(channel, message){
    message = JSON.parse(message);

    io.emit(channel, message.data);
})

server.listen(4200);