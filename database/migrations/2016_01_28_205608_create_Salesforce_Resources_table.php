<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesforceResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SalesforceResources', function (Blueprint $table) {
            $table->increments('id');

		  	$table->integer('user_id')->unsigned();
		  	$table->string('resource_id', 255);
		  	$table->string('resource_name', 255);

            $table->timestamps();

			$table->foreign('user_id')
			      ->references('id')
			  	  ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('SalesforceResources');
    }
}
